package day4

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const boardSize = 5

type board struct {
	data    [boardSize][boardSize]int
	marked  [boardSize][boardSize]bool
	winning bool
}

//containsFalse returns true if the given array of bools contains at least one false value
func containsFalse(s [boardSize]bool) bool {
	for _, v := range s {
		if v == false {
			return true
		}
	}
	return false
}

//wins checks if the board contains a winning streak
func (b *board) wins() {
	for row := 0; row < boardSize; row++ {
		if !containsFalse(b.marked[row]) {
			b.winning = true
		}
	}
	for col := 0; col < boardSize; col++ {
		var s [boardSize]bool
		for row := 0; row < boardSize; row++ {
			s[row] = b.marked[row][col]
		}
		if !containsFalse(s) {
			b.winning = true
		}
	}
}

//Mark marks a number on a board and checks if the board is a winning board
func (b *board) Mark(i int) error {
	for row := 0; row < boardSize; row++ {
		for col := 0; col < boardSize; col++ {
			if b.data[row][col] == i {
				b.marked[row][col] = true
			}
		}
	}
	b.wins()
	return nil
}

//SumOfUnmarked returns the sum of unmarked numbers on the board
func (b *board) SumOfUnmarked() int {
	var sum int
	for row := 0; row < boardSize; row++ {
		for col := 0; col < boardSize; col++ {
			if !b.marked[row][col] {
				sum += b.data[row][col]
			}
		}
	}
	return sum
}

//parseFile parses the file containing input
func parseFile(filepath string) ([]int, []*board) {
	var nums []int
	var numsRead bool
	var boards []*board
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open the file: %w", err))
	}
	defer fd.Close()

	scanner := bufio.NewScanner(fd)
	var current *board
	var posRow int
	for scanner.Scan() {
		if !numsRead {
			var numsString []string
			numsString = strings.Split(scanner.Text(), ",")
			for _, v := range numsString {
				i, err := strconv.Atoi(v)
				if err != nil {
					log.Println(fmt.Errorf("failed to convert string to int: %w", err))
				}
				nums = append(nums, i)
			}
			numsRead = true
			continue
		}
		if scanner.Text() == "" {
			current = &board{}
			continue
		}
		fields := strings.Fields(scanner.Text())
		for posCol, field := range fields {
			i, err := strconv.Atoi(field)
			if err != nil {
				log.Println(fmt.Errorf("failed to convert string to int: %w", err))
			}
			current.data[posRow][posCol] = i
		}
		posRow += 1
		if posRow == boardSize {
			posRow = 0
			boards = append(boards, current)
		}
	}
	if err := scanner.Err(); err != nil {
		log.Println(fmt.Errorf("error when reading file: %w", err))
	}

	return nums, boards
}

//BingoFirstWin runs a bingo game given an input file containing numbers drawn and boards
func BingoFirstWin(filepath string) int {
	nums, boards := parseFile(filepath)
	for _, v := range nums {
		for _, b := range boards {
			b.Mark(v)
			if b.winning {
				sum := b.SumOfUnmarked()
				return sum * v
			}
		}
	}
	log.Println(fmt.Errorf("should never be reached"))
	return 0
}

func BingoLastWin(filepath string) int {
	nums, boards := parseFile(filepath)
	var losing = make(map[int]struct{})

	for i := 0; i < len(boards); i++ {
		losing[i] = struct{}{}
	}

	for _, v := range nums {
		for ind := range losing {
			b := boards[ind]
			b.Mark(v)
			if b.winning {
				// remove rom the list of left boards
				delete(losing, ind)
			}
			if len(losing) == 0 {
				sum := b.SumOfUnmarked()
				return sum * v
			}
		}
	}
	log.Println(fmt.Errorf("should never be reached"))
	return 0
}
