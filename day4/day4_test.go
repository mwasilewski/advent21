package day4

import "testing"

type testCase struct {
	filepath    string
	expected    int
	description string
}

var bingoFirstWinTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    4512,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    51034,
		description: "actual problem",
	},
}

func TestBingoFirstWin(t *testing.T) {
	for _, tc := range bingoFirstWinTestCases {
		actual := BingoFirstWin(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

var bingoLastWinTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    1924,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    5434,
		description: "actual problem",
	},
}

func TestBingoLastWin(t *testing.T) {
	for _, tc := range bingoLastWinTestCases {
		actual := BingoLastWin(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
