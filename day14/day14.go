package day14

import (
	"bufio"
	"compress/zlib"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	bufferSize = 20
	mountpoint = "/media/advent/"
)

type SLLNode struct {
	label rune
	next  *SLLNode
}

func (n *SLLNode) SetLabel(l rune) {
	n.label = l
}

func (n *SLLNode) SetNext(next *SLLNode) {
	n.next = next
}

func (n *SLLNode) CountElements() map[rune]int {
	registry := make(map[rune]int)
	countRecursive(n, registry)
	return registry
}

func InsertAtNode(prev SLLNode, new SLLNode) {
	prev.next, new.next = &new, prev.next
}

func (n *SLLNode) InsertAtEnd(next *SLLNode) {
	if n.next == nil {
		n.next = next
		return
	}
	n.next.InsertAtEnd(next)
}

func countRecursive(n *SLLNode, registry map[rune]int) {
	registry[n.label] += 1
	if n.next == nil {
		return
	}
	countRecursive(n.next, registry)
}

func NewSLLNode() *SLLNode {
	return &SLLNode{}
}

type PairInsertionRule struct {
	pair           []rune
	insertionValue rune
}

func loadDataFromFile(filepath string) (*SLLNode, []PairInsertionRule) {
	var template *SLLNode
	pairs := []PairInsertionRule{}

	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open a file: %w", err))
	}
	defer fd.Close()

	reachedRules := false
	scannerLines := bufio.NewScanner(fd)
	for scannerLines.Scan() {
		line := scannerLines.Text()
		if line == "" {
			reachedRules = true
			continue
		}
		if reachedRules {
			// rules reading logic
			pir := PairInsertionRule{}
			fields := strings.Fields(line)
			for _, v := range []rune(fields[0]) {
				pir.pair = append(pir.pair, v)
			}
			pir.insertionValue = []rune(fields[2])[0]
			pairs = append(pairs, pir)
			continue
		}
		for _, r := range line {
			newN := NewSLLNode()
			newN.SetLabel(r)
			if template == nil {
				template = newN
				continue
			}
			template.InsertAtEnd(newN)

		}
	}
	if err := scannerLines.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan lines: %w", err))
	}

	return template, pairs
}

func maxInRegistry(registry map[rune]int) rune {
	type entry struct {
		r rune
		i int
	}
	max := entry{}
	for k, v := range registry {
		if max.r == 0 {
			max.r = k
			max.i = v
		}
		if v > max.i {
			max.r = k
			max.i = v
		}
	}
	return max.r
}

func minInRegistry(registry map[rune]int) rune {
	type entry struct {
		r rune
		i int
	}
	min := entry{}
	for k, v := range registry {
		if min.r == 0 {
			min.r = k
			min.i = v
		}
		if v < min.i {
			min.r = k
			min.i = v
		}
	}
	return min.r
}

type insertionItem struct {
	prev *SLLNode
	new  *SLLNode
}

type InsertionQueue []insertionItem

func executeInsertion(queue InsertionQueue) {
	for _, item := range queue {
		item.prev.next, item.new.next = item.new, item.prev.next
	}
}

func executeStepRecursive(current *SLLNode, prevNode *SLLNode, pairs []PairInsertionRule, queue *InsertionQueue) {
	if prevNode == nil { // beginning of SLL
		prevNode = current
		executeStepRecursive(current.next, prevNode, pairs, queue)
		return
	}
	for _, rule := range pairs {
		if rule.pair[0] == prevNode.label && rule.pair[1] == current.label {
			newNode := NewSLLNode()
			newNode.SetLabel(rule.insertionValue)
			item := insertionItem{
				prev: prevNode,
				new:  newNode,
			}
			*queue = append(*queue, item)
		}
	}
	if current.next == nil { // end of SLL
		return
	}
	executeStepRecursive(current.next, current, pairs, queue)

}

func step(sll *SLLNode, pairs []PairInsertionRule) {
	var queue InsertionQueue
	executeStepRecursive(sll, nil, pairs, &queue)
	executeInsertion(queue)
}

func diffMostCommonLeastCommon(filepath string, noSteps int) int {
	sll, pairs := loadDataFromFile(filepath)
	for i := 0; i < noSteps; i++ {
		fmt.Printf("step: %d\n", i)
		step(sll, pairs)
	}
	registry := sll.CountElements()
	max := maxInRegistry(registry)
	min := minInRegistry(registry)
	countMax := registry[max]
	countMin := registry[min]

	return countMax - countMin
}

func loadDataToFile(filepath string) (string, []PairInsertionRule) {
	file0 := mountpoint + "file0"
	pairs := []PairInsertionRule{}

	// open file with input
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open a file: %w", err))
	}
	defer fd.Close()

	// open file for step0
	fd0, err := os.OpenFile(file0, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0755)
	if err != nil {
		log.Println(fmt.Errorf("failed to open a file0: %w", err))
	}
	defer fd0.Close()
	compWriter, _ := zlib.NewWriterLevel(fd0, zlib.BestCompression)
	defer compWriter.Close()

	// parsing logic
	reachedRules := false
	scannerLines := bufio.NewScanner(fd)
	for scannerLines.Scan() {
		line := scannerLines.Text()
		if line == "" {
			reachedRules = true
			continue
		}
		if reachedRules {
			// rules reading logic
			pir := PairInsertionRule{}
			fields := strings.Fields(line)
			for _, v := range []rune(fields[0]) {
				pir.pair = append(pir.pair, v)
			}
			pir.insertionValue = []rune(fields[2])[0]
			pairs = append(pairs, pir)
			continue
		}
		fmt.Fprintf(compWriter, line)
	}
	if err := scannerLines.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan lines: %w", err))
	}

	return file0, pairs
}

func processBuffer(compWriter *zlib.Writer, workBuffer []byte, n int, previousByte *byte, beginningOfFile *bool, pairs []PairInsertionRule, registry map[rune]int) {
	// previousByte is a pointer so that the last value gets carried over to the next function call
	//fmt.Printf("workbuffer: %v\n", workBuffer)
out:
	for i := 0; i < n; i++ {
		current := workBuffer[i]
		if *beginningOfFile {
			compWriter.Write([]byte{current}) // write the current byte
			registry[rune(current)] += 1
			*previousByte = current
			*beginningOfFile = false
			continue out
		}
		for _, rule := range pairs {
			if *previousByte == byte(rule.pair[0]) && current == byte(rule.pair[1]) {
				compWriter.Write([]byte{byte(rule.insertionValue)}) // added rune
				registry[rule.insertionValue] += 1
				compWriter.Write([]byte{current}) // write the current byte
				registry[rune(current)] += 1
				*previousByte = current
				continue out // no point in checking further as there are no duplicate rules
			}
		}
		compWriter.Write([]byte{current}) // write the current
		registry[rune(current)] += 1
		*previousByte = current
	}
	err := compWriter.Flush() // force writing this portion of the data
	if err != nil {
		log.Println(fmt.Errorf("failed to write to file: %w", err))
	}
}

// file on disk -> file descriptor in OS -> file descriptor stub in Golang implementing Reader interface -> buffer implementing ByteReader interface, needed to read byte by byte by gzip -> gzip Reader with internal buffer doing uncompressing -> workBuffer which the logic operates on
func stepFile(inputFile string, outputFile string, pairs []PairInsertionRule) map[rune]int {
	registry := make(map[rune]int)
	// input file
	fdIn, err := os.Open(inputFile)
	if err != nil {
		log.Println(fmt.Errorf("failed to open input file: %w", err))
	}
	defer fdIn.Close()
	bFdIn := bufio.NewReader(fdIn)
	compReader, err := zlib.NewReader(bFdIn)
	if err != nil {
		log.Println(fmt.Errorf("failed to create a gzip reader: %w", err))
	}
	defer compReader.Close()

	// output file
	fdOut, err := os.OpenFile(outputFile, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0755)
	if err != nil {
		log.Println(fmt.Errorf("failed to open output file: %w", err))
	}
	defer fdOut.Close()
	//bFdOout := bufio.NewWriter(fdOut)
	compWriter, _ := zlib.NewWriterLevel(fdOut, zlib.BestCompression)
	if err != nil {
		log.Println(fmt.Errorf("failed to create a gzip writer: %w", err))
	}
	defer compWriter.Close()

	// logic
	beginningOfFile := true
	workBuffer := make([]byte, bufferSize)
	var previousB byte // needed to allocate memory so that the pointer is non-nil
	previousByte := &previousB
out:
	for {
		// read into workBuffer
		n, err := compReader.Read(workBuffer)
		if err != nil {
			switch err {
			case io.EOF:
				//log.Println(fmt.Errorf("EOF: %w", err))
				if n != 0 {
					processBuffer(compWriter, workBuffer, n, previousByte, &beginningOfFile, pairs, registry)
				}
			default:
				log.Println(fmt.Sprintf("%T\n", err))
				log.Println(fmt.Errorf("error reading gzip stream: %w", err))

			}
			break out
		}
		processBuffer(compWriter, workBuffer, n, previousByte, &beginningOfFile, pairs, registry)
	}
	return registry
}

func diffMostCommonLeastCommonFile(filepath string, noSteps int) int {
	registry := make(map[rune]int)
	_, pairs := loadDataToFile(filepath)
	for stepInLoop := 1; stepInLoop < noSteps+1; stepInLoop++ {
		fmt.Printf("step: %d\n", stepInLoop)
		inputFileStep := strconv.Itoa(stepInLoop - 1)
		inputFile := mountpoint + "file" + inputFileStep
		outputFileStep := strconv.Itoa(stepInLoop)
		outputFile := mountpoint + "file" + outputFileStep

		registry = stepFile(inputFile, outputFile, pairs)
		fmt.Printf("registry after step: %d : %v\n", stepInLoop, registry)
		fmt.Println()
	}
	max := maxInRegistry(registry)
	min := minInRegistry(registry)
	countMax := registry[max]
	countMin := registry[min]

	return countMax - countMin

}

type Polymer struct {
	firstElement       rune
	bonds              map[string]int
	pairInsertionRules map[string]rune
	elements           map[rune]int
}

func NewPolymer(template string, pairInsertionRules map[string]rune) *Polymer {
	var prev rune
	bonds := make(map[string]int)
	elements := make(map[rune]int)
	for i, r := range template {
		if i == 0 {
			elements[r] += 1
			prev = r
			continue
		}
		pair := string([]rune{prev, r})
		bonds[pair] += 1
		elements[r] += 1
		prev = r
	}
	return &Polymer{
		firstElement:       []rune(template)[0],
		bonds:              bonds,
		pairInsertionRules: pairInsertionRules,
		elements:           elements,
	}

}

func (p *Polymer) maxElement() rune {
	type entry struct {
		r rune
		i int
	}
	max := entry{}
	for k, v := range p.elements {
		if max.r == 0 {
			max.r = k
			max.i = v
		}
		if v > max.i {
			max.r = k
			max.i = v
		}
	}
	return max.r
}

func (p *Polymer) minElement() rune {
	type entry struct {
		r rune
		i int
	}
	min := entry{}
	for k, v := range p.elements {
		if min.r == 0 {
			min.r = k
			min.i = v
		}
		if v < min.i {
			min.r = k
			min.i = v
		}
	}
	return min.r
}

func (p *Polymer) growPolymer() *Polymer {
	newBonds := make(map[string]int)
	newElements := make(map[rune]int)

	newElements[p.firstElement] += 1

	for bond, noBonds := range p.bonds {
		firstNewBond := string([]rune{[]rune(bond)[0], p.pairInsertionRules[bond]})
		secondNewBond := string([]rune{p.pairInsertionRules[bond], []rune(bond)[1]})
		newBonds[firstNewBond] += noBonds
		newElements[[]rune(firstNewBond)[1]] += noBonds
		newBonds[secondNewBond] += noBonds
		newElements[[]rune(secondNewBond)[1]] += noBonds

	}

	newPolymer := &Polymer{
		firstElement:       p.firstElement,
		bonds:              newBonds,
		pairInsertionRules: p.pairInsertionRules,
		elements:           newElements,
	}
	return newPolymer
}

func readPolymerFromFile(filepath string) *Polymer {
	var template string
	pairInsertionRules := make(map[string]rune)

	// open file with input
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open a file: %w", err))
	}
	defer fd.Close()

	// parsing logic
	reachedRules := false
	scannerLines := bufio.NewScanner(fd)
	for scannerLines.Scan() {
		line := scannerLines.Text()
		if line == "" {
			reachedRules = true
			continue
		}
		if reachedRules {
			// rules reading logic
			fields := strings.Fields(line)
			pairInsertionRules[fields[0]] = []rune(fields[2])[0]
			continue
		}
		template = line
	}
	if err := scannerLines.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan lines: %w", err))
	}

	return NewPolymer(template, pairInsertionRules)
}

func diffMostCommonLeastCommonRunes(filepath string, noSteps int) int {
	p := readPolymerFromFile(filepath)
	for i := 1; i < noSteps+1; i++ {
		fmt.Printf("step: %d\n", i)
		p = p.growPolymer()
	}
	maxElement := p.maxElement()
	minElement := p.minElement()
	return p.elements[maxElement] - p.elements[minElement]
}
