package day14

import "testing"

type testCase struct {
	filepath    string
	noSteps     int
	expected    int
	description string
}

var TCsDiffMostCommonLeastCommon = []testCase{
	{
		filepath:    "./test_data/input1",
		noSteps:     1,
		expected:    1,
		description: "example",
	},

	{
		filepath:    "./test_data/input1",
		noSteps:     10,
		expected:    1588,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		noSteps:     10,
		expected:    2321,
		description: "puzzle",
	},

	{
		filepath:    "./test_data/input1",
		noSteps:     21,
		expected:    3942739,
		description: "example",
	},

	{
		filepath:    "./test_data/input1",
		noSteps:     40,
		expected:    2188189693529,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		noSteps:     40,
		expected:    2399822193707,
		description: "puzzle",
	},
}

func TestDiffMostCommonLeastCommon(t *testing.T) {
	for _, tc := range TCsDiffMostCommonLeastCommon {
		actual := diffMostCommonLeastCommon(tc.filepath, tc.noSteps)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

func TestDiffMostCommonLeastCommonFile(t *testing.T) {
	for _, tc := range TCsDiffMostCommonLeastCommon {
		actual := diffMostCommonLeastCommonFile(tc.filepath, tc.noSteps)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

func TestDiffMostCommonLeastCommonRunes(t *testing.T) {
	for _, tc := range TCsDiffMostCommonLeastCommon {
		actual := diffMostCommonLeastCommonRunes(tc.filepath, tc.noSteps)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
