package day18

import (
	"testing"
)

func TestLoadInput(t *testing.T) {
	tcs := []struct {
		filepath    string
		description string
	}{
		{filepath: "./test_data/loadInput/input1", description: "parse example"},
	}
	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			actual := loadInput(tc.filepath)
			if actual == nil {
				t.Errorf("failed to parse anything")
			}
		})
	}
}

func TestMagnitude(t *testing.T) {
	tcs := []struct {
		filepath    string
		expected    int
		description string
	}{
		{filepath: "./test_data/magnitude/input1", expected: 29, description: "simple magnitude"},
		{filepath: "./test_data/magnitude/input2", expected: 21, description: "simple magnitude 2"},
		{filepath: "./test_data/magnitude/input3", expected: 129, description: "simple magnitude 3"},
		{filepath: "./test_data/magnitude/input4", expected: 1384, description: "simple explode"},
		{filepath: "./test_data/magnitude/input7", expected: 1384, description: "simple explode"},
		{filepath: "./test_data/magnitude/input8", expected: 1384, description: "simple explode"},
		{filepath: "./test_data/magnitude/input5", expected: 1384, description: "simple split"},
		{filepath: "./test_data/magnitude/input6", expected: 1384, description: "simple explode and split"},
		{filepath: "./test_data/magnitude/input9", expected: 3488, description: "a bit more complex explode and split"},
		{filepath: "./test_data/magnitude/input10", expected: 143, description: "simple magnitude method test1"},
		{filepath: "./test_data/magnitude/input11", expected: 1384, description: "simple magnitude method test2"},
		{filepath: "./test_data/magnitude/input12", expected: 445, description: "simple magnitude method test3"},
		{filepath: "./test_data/magnitude/input13", expected: 791, description: "simple magnitude method test4"},
		{filepath: "./test_data/magnitude/input14", expected: 1137, description: "simple magnitude method test5"},
		{filepath: "./test_data/magnitude/input15", expected: 3488, description: "simple magnitude method test6"},

		{filepath: "./test_data/magnitude/input16", expected: 4140, description: "simple puzzle"},
		{filepath: "./test_data/magnitude/input17", expected: 3675, description: "actual puzzle"},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			actual := Magnitude(tc.filepath)
			if actual != tc.expected {
				t.Errorf("expected: %d, got: %d", tc.expected, actual)
			}
		})
	}
}
func TestLargestMagnitude(t *testing.T) {
	tcs := []struct {
		filepath    string
		expected    int
		description string
	}{
		{filepath: "./test_data/magnitude/input16", expected: 3993, description: "simple puzzle"},
		{filepath: "./test_data/magnitude/input17", expected: 4650, description: "actual puzzle"},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			actual := LargestMagnitude(tc.filepath)
			if actual != tc.expected {
				t.Errorf("expected: %d, got: %d", tc.expected, actual)
			}
		})
	}
}
