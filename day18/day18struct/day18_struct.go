package day18struct

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type SnailNumber struct {
	first  interface{}
	second interface{}
}

type Literal struct {
	value int
	prev  *Literal
	next  *Literal
}

var last *Literal

func parseLine(reader io.Reader) *SnailNumber {
	sn := &SnailNumber{}

	var buf = make([]byte, 1)
	var numerical []rune

	for {
		_, err := reader.Read(buf)
		if err != nil {
			log.Println(fmt.Errorf("failed to read to buffer: %w", err))
		}
		r := rune(buf[0])

		switch {
		case r == '[':
			snInternal := parseLine(reader)
			if sn.first == nil {
				sn.first = snInternal
			} else {
				sn.second = snInternal
			}
		case unicode.IsDigit(r):
			numerical = append(numerical, r)
		case r == ',':
			if numerical != nil {
				i, err := strconv.Atoi(string(numerical))
				if err != nil {
					log.Println(fmt.Errorf("failed to convert string to integer, first value: %w", err))
				}
				l := &Literal{
					value: i,
					prev:  last,
				}
				sn.first = l
				if last != nil {
					(*last).next = l
				}
				last = l
				numerical = nil
			}
		case r == ']':
			if numerical != nil {
				i, err := strconv.Atoi(string(numerical))
				if err != nil {
					log.Println(fmt.Errorf("failed to convert string to integer, second value: %w", err))
				}
				l := &Literal{
					value: i,
					prev:  last,
				}
				sn.second = l
				if last != nil {
					(*last).next = l
				}
				last = l
				numerical = nil
			}
			return sn
		}
	}

	return sn
}

func loadInput(filepath string) []*SnailNumber {
	// open file
	var res []*SnailNumber
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open file: %w", err))
	}
	defer fd.Close()

	// read and parse lines
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		line := scanner.Text()

		reader := strings.NewReader(string([]rune(line)[1:]))
		sn := parseLine(reader)
		last = nil
		res = append(res, sn)
	}
	if err := scanner.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan line: %w", err))
	}

	// return
	return res
}

func isSnailNumber(in interface{}) bool {
	_, isSN := in.(*SnailNumber)
	if isSN {
		return true
	}
	return false
}

func explode(sn *SnailNumber, lvl int) bool {
	newLvl := lvl + 1
	if lvl < 4 {
		if isSnailNumber(sn.first) {
			if exploded := explode(sn.first.(*SnailNumber), newLvl); exploded {
				newPrev := sn.first.(*SnailNumber).first.(*Literal).prev
				newNext := sn.first.(*SnailNumber).second.(*Literal).next

				newFirst := &Literal{
					value: 0,
					prev:  newPrev,
					next:  newNext,
				}
				sn.first = newFirst
				return true
			}
		}
		if isSnailNumber(sn.second) {
			if exploded := explode(sn.second.(*SnailNumber), newLvl); exploded {
				sn.second = &Literal{
					value: 0,
					prev:  sn.second.(*SnailNumber).first.(*Literal).prev,
					next:  sn.second.(*SnailNumber).second.(*Literal).next,
				}
				return true
			}
		}
	}
	if lvl == 4 {
		literalLeft, _ := sn.first.(*Literal)
		literalRight, _ := sn.second.(*Literal)
		if literalLeft.prev != nil {
			literalLeft.prev.value += literalLeft.value
		}
		if literalRight.next != nil {
			literalRight.next.value += literalRight.value
		}
		return true
	}
	return false
}

func split(sn *SnailNumber) bool {
	//if !isSnailNumber(sn.first) {
	//	i, ok := sn.first.(int)
	//	if !ok {
	//		panic("should never be reached")
	//	}
	//	if i >= 10 {
	//		sn.first = &SnailNumber{
	//			first:  i / 2,
	//			second: math.Ceil(float64(i) / float64(2)),
	//		}
	//		return true
	//	}
	//} else {
	//	if split(sn.first.(*SnailNumber)) {
	//		return true
	//	}
	//}
	//if !isSnailNumber(sn.second) {
	//	i, ok := sn.second.(int)
	//	if !ok {
	//		panic("should never be reached")
	//	}
	//	if i >= 10 {
	//		sn.second = &SnailNumber{
	//			first:  i / 2,
	//			second: math.Ceil(float64(i) / float64(2)),
	//		}
	//	}
	//
	//} else {
	//	if split(sn.second.(*SnailNumber)) {
	//		return true
	//	}
	//}
	//
	return false
}

func reduce(sn *SnailNumber) {
	for {
		if explode(sn, 0) {
			continue
		}
		if split(sn) {
			continue
		}
		break
	}
	return
}

func findFirst(sn *SnailNumber) *Literal {
	if isSnailNumber(sn.first) {
		return findFirst(sn.first.(*SnailNumber))
	}
	if firstLiteral, ok := sn.first.(*Literal); ok {
		if firstLiteral.prev == nil {
			self := firstLiteral.next.prev
			return self
		}
	}
	panic("should never be reached")
}

func findLast(sn *SnailNumber) *Literal {
	if isSnailNumber(sn.second) {
		return findLast(sn.second.(*SnailNumber))
	}
	if secondLiteral, ok := sn.second.(*Literal); ok {
		if secondLiteral.next == nil {
			self := secondLiteral.prev.next
			return self
		}
	}
	panic("Should never be reached")
}

func addSnailNumbers(n1 *SnailNumber, n2 *SnailNumber) *SnailNumber {
	var sn = &SnailNumber{}
	if n1 == nil {
		return n2
	}
	if n2 == nil {
		return n1
	}
	sn.first = n1
	sn.second = n2
	l := findLast(sn.first.(*SnailNumber))
	f := findFirst(sn.second.(*SnailNumber))
	l.next = f
	f.prev = l
	reduce(sn)
	return sn
}

func addListOfSnailNumbers(numbers []*SnailNumber) *SnailNumber {
	var sn *SnailNumber
	for _, number := range numbers {
		sn = addSnailNumbers(sn, number)
	}
	return sn
}

func Magnitude(filepath string) int {
	listOfNumbers := loadInput(filepath)
	snailSum := addListOfSnailNumbers(listOfNumbers)
	fmt.Println(snailSum)

	return 0
}
