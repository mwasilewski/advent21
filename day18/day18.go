package day18

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"unicode"
)

type SnailLiteral struct {
	value int
	prev  *SnailLiteral
	next  *SnailLiteral
}

type SnailBinTreeNode struct {
	lvl        int
	leftChild  interface{}
	rightChild interface{}
	parent     *SnailBinTreeNode
}

func IsSnailBinaryTreeNode(in interface{}) bool {
	_, isSN := in.(*SnailBinTreeNode)
	if isSN {
		return true
	}
	return false
}

func (s *SnailBinTreeNode) BumpLvl(i int) {
	s.lvl += i
	if IsSnailBinaryTreeNode(s.leftChild) {
		s.leftChild.(*SnailBinTreeNode).BumpLvl(i)
	}
	if IsSnailBinaryTreeNode(s.rightChild) {
		s.rightChild.(*SnailBinTreeNode).BumpLvl(i)
	}
}

func (s *SnailBinTreeNode) explode() (bool, bool) {
	if s.lvl == 4 {
		return true, false
	}
	increaseValues := func(explodingS *SnailBinTreeNode) {
		// increase the value to the left
		lv := explodingS.leftChild.(*SnailLiteral).value
		p := explodingS.leftChild.(*SnailLiteral).prev
		if p != nil {
			p.value += lv
		}

		// increase the value to the right
		rv := explodingS.rightChild.(*SnailLiteral).value
		n := explodingS.rightChild.(*SnailLiteral).next
		if n != nil {
			n.value += rv
		}

	}
	for _, child := range []*interface{}{&s.leftChild, &s.rightChild} {
		if IsSnailBinaryTreeNode(*child) {
			childSN := (*child).(*SnailBinTreeNode)
			exploded, editedNodes := childSN.explode()
			if exploded {
				if !editedNodes {
					increaseValues(childSN)
					next := childSN.rightChild.(*SnailLiteral).next
					prev := childSN.leftChild.(*SnailLiteral).prev

					// replace with a zero value
					*child = &SnailLiteral{
						value: 0,
						prev:  prev,
						next:  next,
					}

					if next != nil {
						next.prev = (*child).(*SnailLiteral)
					}
					if prev != nil {
						prev.next = (*child).(*SnailLiteral)
					}

					editedNodes = true
				}
				return exploded, editedNodes
			}
			continue
		}
	}
	return false, false
}

func (s *SnailBinTreeNode) split() bool {
	var splitted bool
	for _, child := range []*interface{}{&s.leftChild, &s.rightChild} {
		if IsSnailBinaryTreeNode(*child) {
			splitted = (*child).(*SnailBinTreeNode).split()
			if splitted {
				return splitted
			}
			continue
		}
		childLit := (*child).(*SnailLiteral)
		if childLit.value >= 10 {
			leftV := childLit.value / 2
			leftChild := &SnailLiteral{
				value: leftV,
				prev:  childLit.prev,
				next:  nil,
			}

			rightV := int(math.Ceil(float64(childLit.value) / float64(2)))
			rightChild := &SnailLiteral{
				value: rightV,
				prev:  nil,
				next:  childLit.next,
			}

			leftChild.next = rightChild
			rightChild.prev = leftChild
			if childLit.prev != nil {
				childLit.prev.next = leftChild
			}
			if childLit.next != nil {
				childLit.next.prev = rightChild
			}

			newNode := &SnailBinTreeNode{
				lvl:        s.lvl + 1,
				leftChild:  leftChild,
				rightChild: rightChild,
				parent:     s,
			}
			*child = newNode

			splitted = true
			return splitted
		}
	}

	return false
}

func (s *SnailBinTreeNode) Reduce() {
	for {
		if exploded, _ := s.explode(); exploded {
			continue
		}
		if s.split() {
			continue
		}
		break
	}
	return
}

func (s *SnailBinTreeNode) printAllLiteralValues() {
	current := findFirst(s)
	result := []int{}
	for {
		if current.next == nil {
			result = append(result, current.value)
			break
		}
		result = append(result, current.value)
		current = current.next
	}

	fmt.Println(result)
}

func (s SnailBinTreeNode) magnitude() int {
	var res int
	if IsSnailBinaryTreeNode(s.leftChild) {
		res += 3 * s.leftChild.(*SnailBinTreeNode).magnitude()
	} else {
		res += 3 * s.leftChild.(*SnailLiteral).value
	}
	if IsSnailBinaryTreeNode(s.rightChild) {
		res += 2 * s.rightChild.(*SnailBinTreeNode).magnitude()
	} else {
		res += 2 * s.rightChild.(*SnailLiteral).value
	}
	return res
}

func Rtoi(runes []rune) int {
	res, err := strconv.Atoi(string(runes))
	if err != nil {
		panic("failed to convert runes to integer")
	}
	return res
}

func parseLine(line string) *SnailBinTreeNode {
	lvl := 0
	var sn = &SnailBinTreeNode{
		lvl: lvl,
	}
	var pointer = sn
	var lastLiteral *SnailLiteral
	numLiteral := []rune{}
	runes := []rune(line)
	for i, r := range runes {
		if i == 0 {
			continue
		}
		switch {
		case r == '[':
			lvl += 1
			child := &SnailBinTreeNode{
				parent: pointer,
				lvl:    lvl,
			}
			if runes[i-1] == '[' {
				pointer.leftChild = child
				child.parent = pointer
				pointer = child
				continue
			}
			if runes[i-1] == ',' {
				pointer.rightChild = child
				child.parent = pointer
				pointer = child
				continue
			}
		case unicode.IsDigit(r):
			numLiteral = append(numLiteral, r)
		case r == ',':
			if len(numLiteral) != 0 {
				value := Rtoi(numLiteral)
				numLiteral = []rune{}

				newSnailLiteral := &SnailLiteral{value: value}
				if lastLiteral != nil {
					lastLiteral.next = newSnailLiteral
					newSnailLiteral.prev = lastLiteral
				}
				lastLiteral = newSnailLiteral
				pointer.leftChild = newSnailLiteral
				continue
			}
			if runes[i-1] == ']' && pointer.parent != nil {
				lvl = pointer.parent.lvl
				pointer = pointer.parent
			}
		case r == ']':
			if len(numLiteral) != 0 {
				value := Rtoi(numLiteral)
				numLiteral = []rune{}

				newSnailLiteral := &SnailLiteral{value: value}
				if lastLiteral != nil {
					lastLiteral.next = newSnailLiteral
					newSnailLiteral.prev = lastLiteral
				}
				lastLiteral = newSnailLiteral
				pointer.rightChild = newSnailLiteral
				continue
			}
			if pointer.parent != nil {
				lvl = pointer.parent.lvl
				pointer = pointer.parent
			}
		}
	}
	return sn
}

func loadInput(filepath string) []*SnailBinTreeNode {

	var res []*SnailBinTreeNode

	// open file
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open file: %w", err))
	}
	defer fd.Close()

	// read lines and parse them
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		line := scanner.Text()
		sn := parseLine(line)
		res = append(res, sn)
	}
	if err := scanner.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan line: %w", err))
	}

	// return
	return res
}

func findFirst(sn *SnailBinTreeNode) *SnailLiteral {
	if IsSnailBinaryTreeNode(sn.leftChild) {
		return findFirst(sn.leftChild.(*SnailBinTreeNode))
	}
	if firstLiteral, ok := sn.leftChild.(*SnailLiteral); ok {
		if firstLiteral.prev == nil {
			self := firstLiteral.next.prev
			return self
		}
		log.Println("the first item still points to non-nil value")
		panic("should never be reached")
	}
	return nil
}

func findLast(sn *SnailBinTreeNode) *SnailLiteral {
	if IsSnailBinaryTreeNode(sn.rightChild) {
		return findLast(sn.rightChild.(*SnailBinTreeNode))
	}
	if lastLiteral, ok := sn.rightChild.(*SnailLiteral); ok {
		if lastLiteral.next == nil {
			self := lastLiteral.prev.next
			return self
		}
		log.Println("the last item still points to the non-nil value")
		panic("Should never be reached")
	}
	return nil
}

func AddSnailBinTrees(n1 *SnailBinTreeNode, n2 *SnailBinTreeNode) *SnailBinTreeNode {
	if n1 == nil {
		return n2
	}
	if n2 == nil {
		return n1
	}
	var sn = &SnailBinTreeNode{
		lvl:        -1,
		leftChild:  n1,
		rightChild: n2,
		parent:     nil,
	}
	sn.BumpLvl(1)

	last := findLast(sn.leftChild.(*SnailBinTreeNode))
	first := findFirst(sn.rightChild.(*SnailBinTreeNode))
	last.next = first
	first.prev = last

	sn.Reduce()
	return sn
}

func SumSnailBinTreesSlice(numbers []*SnailBinTreeNode) *SnailBinTreeNode {
	var sn *SnailBinTreeNode
	for _, number := range numbers {
		sn = AddSnailBinTrees(sn, number)
	}
	return sn
}

func Magnitude(filepath string) int {
	snailNumbersSlice := loadInput(filepath)
	reducedSnailNumber := SumSnailBinTreesSlice(snailNumbersSlice)
	res := reducedSnailNumber.magnitude()
	return res
}

func pairsPermutations(lines int) [][]int {
	var res [][]int
	for i := 0; i < lines-1; i++ {
		for j := i + 1; j < lines; j++ {
			res = append(res, []int{i, j})
			res = append(res, []int{j, i})
		}
	}
	return res
}

func readLines(filepath string) []string {
	var res []string

	// open file
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open file: %w", err))
	}
	defer fd.Close()

	// read lines
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		line := scanner.Text()
		res = append(res, line)
	}
	if err := scanner.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan line: %w", err))
	}

	// return
	return res
}

func LargestMagnitude(filepath string) int {
	lines := readLines(filepath)
	p := pairsPermutations(len(lines))
	var max *int
	for _, pair := range p {
		first := lines[pair[0]]
		second := lines[pair[1]]
		firstSN := parseLine(first)
		secondSN := parseLine(second)
		reducedSN := SumSnailBinTreesSlice([]*SnailBinTreeNode{firstSN, secondSN})
		mag := reducedSN.magnitude()
		if max == nil || *max < mag {
			max = &mag
		}
	}
	return *max
}
