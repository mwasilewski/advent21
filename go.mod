module gitlab.com/mwasilewski/advent21

go 1.17

require (
	github.com/barkimedes/go-deepcopy v0.0.0-20200817023428-a044a1957ca4
	github.com/go-kit/log v0.2.0
	github.com/lib/pq v1.10.4
)

require github.com/go-logfmt/logfmt v0.5.1 // indirect
