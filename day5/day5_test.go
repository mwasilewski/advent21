package day5

import "testing"

type testCase struct {
	filepath    string
	expected    int
	description string
}

var ventsMapHorizVertTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    5,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    7297,
		description: "actual problem",
	},
}

func TestVentsMapHorizVert(t *testing.T) {
	for _, tc := range ventsMapHorizVertTestCases {
		actual := ventsMapHorizVert(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

var ventsMapHorizVertDiagTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    12,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    21038,
		description: "actual problem",
	},
}

func TestVentsMapHorizVertDiag(t *testing.T) {
	for _, tc := range ventsMapHorizVertDiagTestCases {
		actual := ventsMapHorizVertDiag(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
