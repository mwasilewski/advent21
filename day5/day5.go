package day5

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const ventsThreshold = 2

type point struct {
	x int
	y int
}

type line struct {
	points    map[int]point
	nextAvail int
}

func (l *line) appendPoint(p point) {
	l.points[l.nextAvail] = p
	l.nextAvail += 1
}

func getMaxX(lines []line) int {
	var max int
	for _, l := range lines {
		for _, p := range l.points {
			if p.x > max {
				max = p.x
			}
		}
	}
	// points are indexed from 0
	return max + 1
}

func getMaxY(lines []line) int {
	var max int
	for _, l := range lines {
		for _, p := range l.points {
			if p.y > max {
				max = p.y
			}
		}
	}
	// points are indexed from 0
	return max + 1
}

type diagram struct {
	data       [][]int
	maxX       int
	maxY       int
	plotterPos point
}

func (d *diagram) draw() {
	d.data[d.plotterPos.x][d.plotterPos.y] += 1
}

func newDiagram(lines []line) *diagram {
	d := diagram{}
	d.maxX = getMaxX(lines)
	d.maxY = getMaxY(lines)

	// preallocate memory for the data
	data := make([][]int, d.maxX)
	for i := range data {
		data[i] = make([]int, d.maxY)
	}
	d.data = data

	// fill diagram with data
	for _, l := range lines {
		// we start plotting the line from the first point in line definition
		d.plotterPos = l.points[0]
		d.draw()
		// for each next point
		for i := 1; i < len(l.points); i++ {
			xdiff := l.points[i].x - d.plotterPos.x
			ydiff := l.points[i].y - d.plotterPos.y
			for {
				if xdiff == 0 && ydiff == 0 {
					break
				}
				if xdiff != 0 {
					if xdiff < 0 {
						d.plotterPos.x -= 1
						xdiff += 1
					}
					if xdiff > 0 {
						d.plotterPos.x += 1
						xdiff -= 1
					}
				}
				if ydiff != 0 {
					if ydiff < 0 {
						d.plotterPos.y -= 1
						ydiff += 1
					}
					if ydiff > 0 {
						d.plotterPos.y += 1
						ydiff -= 1
					}
				}
				d.draw()
			}
		}
	}

	return &d
}

func (d *diagram) overlapping(threshold int) int {
	var cnt int
	for x := 0; x < d.maxX; x++ {
		for y := 0; y < d.maxY; y++ {
			if d.data[x][y] >= threshold {
				cnt += 1
			}
		}
	}
	return cnt
}

func parseInput(filepath string) []line {
	// open file
	fd, err := os.Open(filepath)
	if err != nil {
		fmt.Println(fmt.Errorf("failed to open file: %w", err))
	}
	defer fd.Close()

	// read file line by line
	var res []line
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		l := line{}
		l.points = make(map[int]point)
		fields := strings.Fields(scanner.Text())
		// for each "word" in a line of input
		for _, field := range fields {
			if field == "->" {
				continue
			}
			var p point
			// parse a word in a line (point)
			for j, no := range strings.Split(field, ",") {
				i, err := strconv.Atoi(no)
				if err != nil {
					fmt.Println(fmt.Errorf("failed to convert rune to integer: %w", err))
				}
				// we assume that the first number in a point is the x coordinate
				if j == 0 {
					p.x = i
					continue
				}
				p.y = i
				l.appendPoint(p)
			}

		}
		res = append(res, l)
	}
	if err := scanner.Err(); err != nil {
		fmt.Println(fmt.Errorf("failed to scan lines: %w", err))
	}
	return res
}

func keepHorizVert(lines []line) []line {
	var res []line
	for _, l := range lines {
		// assumes lines consist of two points
		if l.points[0].x != l.points[1].x && l.points[0].y != l.points[1].y {
			continue
		}
		res = append(res, l)
	}
	return res
}

func ventsMapHorizVert(filepath string) int {
	var lines []line
	lines = parseInput(filepath)
	lines = keepHorizVert(lines)
	d := newDiagram(lines)
	o := d.overlapping(ventsThreshold)
	return o
}

func ventsMapHorizVertDiag(filepath string) int {
	var lines []line
	lines = parseInput(filepath)
	d := newDiagram(lines)
	o := d.overlapping(ventsThreshold)
	return o
}
