#!/bin/bash

docker stop some-postgres && docker rm --volumes some-postgres && docker run --name some-postgres -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres:13 && sleep 10

PGPASSWORD=mysecretpassword psql -h 127.0.0.1 -p 5432 -U postgres -c "create database example;"

POSTGRESQL_URL='postgres://postgres:mysecretpassword@localhost:5432/example?sslmode=disable'

migrate -database ${POSTGRESQL_URL} -path db/migration up
