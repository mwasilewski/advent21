CREATE TABLE IF NOT EXISTS fish(
    id bigserial PRIMARY KEY,
    days_left bigint NOT NULL,
    day_processed bigint NOT NULL
);
