package day6

import "testing"

type testCase struct {
	filepath    string
	noDays      int
	expected    int
	description string
}

var fishPopulationTestCases = []testCase{
	//{
	//	filepath:    "./test_data/input1",
	//	noDays:      18,
	//	expected:    26,
	//	description: "example",
	//},

	//{
	//	filepath:    "./test_data/input1",
	//	noDays:      80,
	//	expected:    5934,
	//	description: "example",
	//},

	{
		filepath:    "./test_data/input2",
		noDays:      80,
		expected:    374994,
		description: "actual problem",
	},
}

func TestFishPopulation(t *testing.T) {
	for _, tc := range fishPopulationTestCases {
		actual := fishPopulation(tc.filepath, tc.noDays)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
