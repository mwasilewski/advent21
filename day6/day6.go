package day6

import (
	"bufio"
	"bytes"
	"database/sql"
	"fmt"
	"github.com/go-kit/log"
	"github.com/go-kit/log/level"
	_ "github.com/lib/pq"
	"os"
	"strconv"
	"strings"
)

const (
	sqlConnInfo      = "host=127.0.0.1 port=5432 user=postgres password=mysecretpassword dbname=example sslmode=disable"
	interval         = 6 // because this is 0 indexed
	startingInterval = interval + 2
	startingDay      = 0

	sqlTableName    = "fish"
	sqlDaysLeft     = "days_left"
	sqlDayProcessed = "day_processed"
)

type fish struct {
	id           int
	daysLeft     int
	dayProcessed int
}

func SplitAt(substring string) func(data []byte, atEOF bool) (advance int, token []byte, err error) {
	searchBytes := []byte(substring)
	searchLen := len(searchBytes)
	return func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		dataLen := len(data)

		// Return nothing if at end of file and no data passed
		if atEOF && dataLen == 0 {
			return 0, nil, nil
		}

		// Find next separator and return token
		if i := bytes.Index(data, searchBytes); i >= 0 {
			return i + searchLen, data[0:i], nil
		}

		// If we're at EOF, we have a final, non-terminated line. Return it.
		if atEOF {
			return dataLen, data, nil
		}

		// Request more data.
		return 0, nil, nil
	}
}

func addFish(logger log.Logger, db *sql.DB, f fish) {
	sqlStmt := "INSERT INTO " + sqlTableName + " (" + sqlDaysLeft + ", " + sqlDayProcessed + ") VALUES ($1, $2)"
	_, err := db.Exec(sqlStmt, f.daysLeft, f.dayProcessed)
	if err != nil {
		level.Error(logger).Log("err", fmt.Errorf("failed to insert fish: %w", err))
	}

}

func loadInput(filepath string, logger log.Logger, db *sql.DB) {
	fd, err := os.Open(filepath)
	if err != nil {
		level.Error(logger).Log("err", fmt.Errorf("Failed to open file %w", err))
	}
	defer fd.Close()

	scannerLine := bufio.NewScanner(fd)
	scannerLine.Scan()
	r := bytes.NewReader(scannerLine.Bytes())
	scannerFish := bufio.NewScanner(r)
	scannerFish.Split(SplitAt(","))
	for scannerFish.Scan() {
		f := fish{}
		f.daysLeft, err = strconv.Atoi(scannerFish.Text())
		if err != nil {
			level.Error(logger).Log("err", fmt.Errorf("Failed to convert string to int: %w", err))
		}
		f.dayProcessed = startingDay

		addFish(logger, db, f)
	}
	if err := scannerFish.Err(); err != nil {
		level.Error(logger).Log("err", fmt.Errorf("error scanning file: %w", err))
	}

}

func growPopulation(logger log.Logger, db *sql.DB, noDays int) {
	var transaction strings.Builder
	transactionBegin := `
BEGIN;
`
	transactionEnd := `
COMMIT;
`
	dayUpdateStmt := `
update fish
set days_left = %d, day_processed = %d
where days_left = %d and day_processed = %d;
`
	getFishWithZero := `
SELECT count(*) FROM fish WHERE days_left = 0;
`
	newFish := `
INSERT INTO fish (days_left, day_processed)
VALUES (%d, %d);
`

	for i := 1; i < noDays+1; i++ {
		level.Info(logger).Log("msg", "Processing day", "day", i)

		transaction.WriteString(transactionBegin)

		// days_left = 0
		var count int
		err := db.QueryRow(getFishWithZero).Scan(&count)
		if err != nil {
			level.Error(logger).Log("err", fmt.Errorf("failed to get number of fish with 0: %w", err))
		}
		for j := 0; j < count; j++ {
			transaction.WriteString(fmt.Sprintf(newFish, startingInterval, i))
		}
		transaction.WriteString(fmt.Sprintf(dayUpdateStmt, interval, i, 0, i-1))

		// days_left > 0
		for j := interval + 2; j > 0; j-- {
			transaction.WriteString(fmt.Sprintf(dayUpdateStmt, j-1, i, j, i-1))
		}

		transaction.WriteString(transactionEnd)

		// execute transaction
		level.Debug(logger).Log("query", transaction.String())
		_, err = db.Query(transaction.String())
		if err != nil {
			level.Error(logger).Log("err", fmt.Errorf("failed to query db: %w", err))
		}

		transaction.Reset()

	}
}

func noFish(logger log.Logger, db *sql.DB) int {
	var count int
	sqlStmt := "SELECT COUNT(*) FROM " + sqlTableName + ";"
	row := db.QueryRow(sqlStmt)
	err := row.Scan(&count)
	if err != nil {
		level.Error(logger).Log("err", fmt.Errorf("failed to parse row returned: %w", err))
	}
	return count
}

func fishPopulation(filepath string, noDays int) int {
	var (
		db     *sql.DB
		logger log.Logger
		err    error
	)
	// init logger
	logger = log.NewJSONLogger(log.NewSyncWriter(os.Stderr))
	logger = level.NewFilter(logger, level.AllowInfo())
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)
	level.Debug(logger).Log("msg", "Finished initializing logging.")

	// init db
	db, err = sql.Open("postgres", sqlConnInfo)
	if err != nil {
		level.Error(logger).Log("err", fmt.Errorf("Failed to connect to db %w", err))
	}
	defer db.Close()

	// actual logic
	loadInput(filepath, logger, db)
	fmt.Println(noFish(logger, db))
	growPopulation(logger, db, noDays)
	return noFish(logger, db)
}
