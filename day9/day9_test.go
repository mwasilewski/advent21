package day9

import "testing"

type testCase struct {
	filepath    string
	expected    int
	description string
}

var sumRiskLevelsTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    15,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    500,
		description: "actual puzzle",
	},
}

func TestSumRiskLevels(t *testing.T) {
	for _, tc := range sumRiskLevelsTestCases {
		actual := sumRiskLevels(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

var multiplyBiggestBasinsTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    1134,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    970200,
		description: "actual puzzle",
	},
}

func TestMultiplybiggestBasins(t *testing.T) {
	for _, tc := range multiplyBiggestBasinsTestCases {
		actual := multiplyBiggestBasins(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
