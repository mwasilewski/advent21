package day9

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
)

type point struct {
	row int
	col int
}

type points []point

func (ps points) present(pIn point) bool {
	for _, p := range ps {
		if p.row == pIn.row && p.col == pIn.col {
			return true
		}
	}
	return false
}

type heightMap [][]int // [y][x]int  or [row][col]int

func (hm heightMap) left(row int, col int) (point, error) {
	if col == 0 {
		return point{}, fmt.Errorf("at the left edge of the map")
	}
	return point{row: row, col: col - 1}, nil
}

func (hm heightMap) right(row int, col int) (point, error) {
	if col == len(hm[0])-1 {
		return point{}, fmt.Errorf("at the right edge of the map")
	}
	return point{row: row, col: col + 1}, nil
}

func (hm heightMap) up(row int, col int) (point, error) {
	if row == 0 {
		return point{}, fmt.Errorf("at the top edge of the map")
	}
	return point{row: row - 1, col: col}, nil
}

func (hm heightMap) down(row int, col int) (point, error) {
	if row == len(hm)-1 {
		return point{}, fmt.Errorf("at the bottom edge of the map")
	}
	return point{row: row + 1, col: col}, nil
}

func loadData(filepath string) heightMap {
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open a file: %w", err))
	}
	defer fd.Close()

	var hm heightMap
	scannerLines := bufio.NewScanner(fd)
	for scannerLines.Scan() {
		var l []int
		for _, r := range scannerLines.Text() {
			l = append(l, int(r-'0'))
		}
		hm = append(hm, l)
	}
	if err := scannerLines.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan lines: %w", err))
	}
	return hm
}

func lowPoints(hm heightMap) points {
	var lowPoints points
	add := func(row int, col int) {
		lowPoints = append(lowPoints, point{row: row, col: col})
	}
	lenRow := len(hm)
	lenCol := len(hm[0])
	for rowInd, row := range hm {
		for colInd, val := range row {
			switch rowInd {
			case 0: // top row
				if colInd == 0 {
					// upper left corner
					if val < hm[rowInd][colInd+1] && val < hm[rowInd+1][colInd] {
						add(rowInd, colInd)
					}
					continue
				}
				if colInd == lenCol-1 {
					// upper right corner
					if val < hm[rowInd][colInd-1] && val < hm[rowInd+1][colInd] {
						add(rowInd, colInd)
					}
					continue
				}
				// middle cols at the top
				if val < hm[rowInd][colInd-1] && val < hm[rowInd][colInd+1] && val < hm[rowInd+1][colInd] {
					add(rowInd, colInd)
				}
				continue
			case lenRow - 1: // bottom row
				if colInd == 0 {
					// lower left corner
					if val < hm[rowInd-1][colInd] && val < hm[rowInd][colInd+1] {
						add(rowInd, colInd)
					}
					continue
				}
				if colInd == lenCol-1 {
					// lower right corner
					if val < hm[rowInd-1][colInd] && val < hm[rowInd][colInd-1] {
						add(rowInd, colInd)
					}
					continue
				}
				// middle cols at the bottom
				if val < hm[rowInd][colInd-1] && val < hm[rowInd-1][colInd] && val < hm[rowInd][colInd+1] {
					add(rowInd, colInd)
				}
				continue
			default: // middle rows
				if colInd == 0 {
					// left end of middle rows
					if val < hm[rowInd-1][colInd] && val < hm[rowInd][colInd+1] && val < hm[rowInd+1][colInd] {
						add(rowInd, colInd)
					}
					continue
				}
				if colInd == lenCol-1 {
					// right end of middle rows
					if val < hm[rowInd-1][colInd] && val < hm[rowInd][colInd-1] && val < hm[rowInd+1][colInd] {
						add(rowInd, colInd)
					}
					continue
				}
				// middle cols of middle rows
				if val < hm[rowInd][colInd+1] && val < hm[rowInd+1][colInd] && val < hm[rowInd][colInd-1] && val < hm[rowInd-1][colInd] {
					add(rowInd, colInd)
				}
				continue
			}
		}
	}
	return lowPoints
}

func riskLevel(hm heightMap, row int, col int) int {
	return 1 + hm[row][col]
}

func sumRiskLevels(filepath string) int {
	var sum int
	hm := loadData(filepath)
	lp := lowPoints(hm)
	for _, p := range lp {
		sum += riskLevel(hm, p.row, p.col)
	}
	return sum
}

func basinSizes(hm heightMap, lp points) []int {
	var bsizes []int
	for _, lowPoint := range lp {
		var endOfBasin = false
		basin := points{
			point{
				row: lowPoint.row,
				col: lowPoint.col,
			},
		}
		newPoints := points{
			point{
				row: lowPoint.row,
				col: lowPoint.col,
			},
		}
		for !endOfBasin {
			if len(newPoints) == 0 {
				endOfBasin = true
				continue
			}
			nextRunNewPoints := points{}
			for _, p := range newPoints {
				// right
				right, err := hm.right(p.row, p.col)
				if err != nil {
					log.Println(err)
				}
				if err == nil && hm[right.row][right.col] < 9 && !basin.present(right) {
					nextRunNewPoints = append(nextRunNewPoints, right)
					basin = append(basin, right)
				}
				// left
				left, err := hm.left(p.row, p.col)
				if err != nil {
					log.Println(err)
				}
				if err == nil && hm[left.row][left.col] < 9 && !basin.present(left) {
					nextRunNewPoints = append(nextRunNewPoints, left)
					basin = append(basin, left)
				}
				// up
				up, err := hm.up(p.row, p.col)
				if err != nil {
					log.Println(err)
				}
				if err == nil && hm[up.row][up.col] < 9 && !basin.present(up) {
					nextRunNewPoints = append(nextRunNewPoints, up)
					basin = append(basin, up)
				}
				// down
				down, err := hm.down(p.row, p.col)
				if err != nil {
					log.Println(err)
				}
				if err == nil && hm[down.row][down.col] < 9 && !basin.present(down) {
					nextRunNewPoints = append(nextRunNewPoints, down)
					basin = append(basin, down)
				}
			}
			newPoints = nextRunNewPoints
		}
		bsizes = append(bsizes, len(basin))
	}
	return bsizes
}

func multiplyBiggestBasins(filepath string) int {
	var mult = 1
	hm := loadData(filepath)
	lp := lowPoints(hm)
	bsizes := basinSizes(hm, lp)
	sort.Ints(bsizes)
	for _, v := range bsizes[len(bsizes)-3:] {
		mult = mult * v
	}
	return mult
}
