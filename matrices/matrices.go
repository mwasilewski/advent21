package matrices

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type Matrix [][]int

type Point struct {
	X int
	Y int
}

func NewMatrix(cols, rows int) Matrix {
	var result Matrix
	for y := 0; y < rows; y++ {
		row := []int{}
		for x := 0; x < cols; x++ {
			row = append(row, 0) // zero value
		}
		result = append(result, row)
	}
	return result
}

func NewMatrixBool(cols, rows int) [][]bool {
	var result [][]bool
	for y := 0; y < rows; y++ {
		row := []bool{}
		for x := 0; x < cols; x++ {
			row = append(row, false) // zero value
		}
		result = append(result, row)
	}
	return result
}

//NewDigitsFromFile where [y][x]int or [row][col]int
func (m *Matrix) NewDigitsFromFile(filepath string) {
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open a file: %w", err))
	}
	defer fd.Close()

	scannerLines := bufio.NewScanner(fd)
	for scannerLines.Scan() {
		var l []int
		for _, r := range scannerLines.Text() {
			l = append(l, int(r-'0'))
		}
		*m = append(*m, l)
	}
	if err := scannerLines.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan lines: %w", err))
	}
}

func Equal(a []int, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for k, v := range a {
		if v != b[k] {
			return false
		}
	}
	return true
}

func (m Matrix) Min() *Point {
	min := new(Point)
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[0]); x++ {
			if y == 0 && x == 0 {
				min.X = x
				min.Y = y
			}
			if m[y][x] < m[min.Y][min.X] {
				min.X = x
				min.Y = y
			}
		}
	}
	return min
}

func (m Matrix) NoRows() int {
	return len(m)
}

func (m Matrix) NoCols() int {
	return len(m[0])
}

func (m Matrix) CountAbove(i int) int {
	var cnt int
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[0]); x++ {
			if m[y][x] > i {
				cnt += 1
			}
		}
	}
	return cnt
}

func (m Matrix) AddToAll(i int) {
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[0]); x++ {
			m[y][x] += i
		}
	}
}

func (m Matrix) Trim(threshold int, newValue int) {
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[0]); x++ {
			if m[y][x] > threshold {
				m[y][x] = newValue
			}
		}
	}
}

func (m Matrix) TrimSubtract(threshold int, subtract int) {
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[0]); x++ {
			if m[y][x] > threshold {
				m[y][x] = m[y][x] - subtract
			}
		}
	}
}

func (m Matrix) Up(p *Point) (*Point, error) {
	if p.Y == 0 {
		return nil, fmt.Errorf("at the top edge of the map")
	}
	return &Point{
		X: p.X,
		Y: p.Y - 1,
	}, nil
}

func (m Matrix) UpperRight(p *Point) (*Point, error) {
	if p.Y == 0 {
		return nil, fmt.Errorf("at the top edge of the map")
	}
	if p.X == len(m[0])-1 {
		return nil, fmt.Errorf("at the right edge of the map")
	}
	return &Point{
		X: p.X + 1,
		Y: p.Y - 1,
	}, nil
}

func (m Matrix) Right(p *Point) (*Point, error) {
	if p.X == len(m[0])-1 {
		return nil, fmt.Errorf("at the right edge of the map")
	}
	return &Point{
		X: p.X + 1,
		Y: p.Y,
	}, nil
}

func (m Matrix) LowerRight(p *Point) (*Point, error) {
	if p.Y == len(m)-1 {
		return nil, fmt.Errorf("at the bottom edge of the map")
	}
	if p.X == len(m[0])-1 {
		return nil, fmt.Errorf("at the right edge of the map")
	}
	return &Point{
		X: p.X + 1,
		Y: p.Y + 1,
	}, nil
}

func (m Matrix) Down(p *Point) (*Point, error) {
	if p.Y == len(m)-1 {
		return nil, fmt.Errorf("at the bottom edge of the map")
	}
	return &Point{
		X: p.X,
		Y: p.Y + 1,
	}, nil
}

func (m Matrix) LowerLeft(p *Point) (*Point, error) {
	if p.Y == len(m)-1 {
		return nil, fmt.Errorf("at the bottom edge of the map")
	}
	if p.X == 0 {
		return nil, fmt.Errorf("at the left edge of the map")
	}
	return &Point{
		X: p.X - 1,
		Y: p.Y + 1,
	}, nil
}

func (m Matrix) Left(p *Point) (*Point, error) {
	if p.X == 0 {
		return nil, fmt.Errorf("at the left edge of the map")
	}
	return &Point{
		X: p.X - 1,
		Y: p.Y,
	}, nil
}

func (m Matrix) UpperLeft(p *Point) (*Point, error) {
	if p.Y == 0 {
		return nil, fmt.Errorf("at the top edge of the map")
	}
	if p.X == 0 {
		return nil, fmt.Errorf("at the left edge of the map")
	}
	return &Point{
		X: p.X - 1,
		Y: p.Y - 1,
	}, nil
}

func (m Matrix) GetNeighbors(p *Point, directions []func(matrix Matrix, p *Point) (*Point, error)) []*Point {
	var neighbors []*Point
	for _, direction := range directions {
		neighbor, err := direction(m, p)
		if err != nil {
			continue
		}
		neighbors = append(neighbors, neighbor)
	}
	return neighbors
}

func (m Matrix) Submatrix(upperLeft *Point, lowerRight *Point) Matrix {
	var sub Matrix
	for y := upperLeft.Y; y < lowerRight.Y+1; y++ {
		subRow := []int{}
		for x := upperLeft.X; x < lowerRight.X+1; x++ {
			subRow = append(subRow, m[y][x])
		}
		sub = append(sub, subRow)
	}
	return sub
}

// along y axis, rows
func (m Matrix) MirrorHorizontally() Matrix {
	var mirrored Matrix
	for i := len(m) - 1; i > -1; i-- {
		mirrored = append(mirrored, m[i])
	}
	return mirrored
}

// along x axis, cols
func (m Matrix) MirrorVertically() Matrix {
	var mirrored = NewMatrix(len(m[0]), len(m))
	// the mirroring itself
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[0]); x++ {
			mirrored[y][len(m[0])-x-1] = m[y][x]
		}
	}

	return mirrored
}

func (m Matrix) Modulo(mod int) {
	for y := 0; y < len(m); y++ {
		for x := 0; x < len(m[0]); x++ {
			m[y][x] = m[y][x] % mod
		}
	}

}

// matrices can have different dimensions
func AddMatrices(m1 Matrix, m2 Matrix, offset *Point) Matrix {
	var xMax int
	var yMax int
	if m1.NoRows() >= m2.NoRows() {
		yMax = m1.NoRows()
	} else {
		yMax = m2.NoRows()
	}
	if m1.NoCols() >= m2.NoCols() {
		xMax = m1.NoCols()
	} else {
		xMax = m2.NoCols()
	}
	res := NewMatrix(xMax, yMax)

	for y := 0; y < len(res); y++ {
		for x := 0; x < len(res[0]); x++ {
			if offset.X < 0 || offset.Y < 0 {
				// m1 is smaller than m2
				smallerMatrixY := offset.Y + y
				smallerMatrixX := offset.X + x
				if smallerMatrixX >= 0 && smallerMatrixY >= 0 {
					res[y][x] += m1[smallerMatrixY][smallerMatrixX] + m2[x][y]
				}
				res[y][x] += m2[y][x]
			} else {
				// m1 is equal or bigger than m2
				smallerMatrixY := y - offset.Y
				smallerMatrixX := x - offset.X
				if smallerMatrixX >= 0 && smallerMatrixY >= 0 && smallerMatrixX < m2.NoCols() && smallerMatrixY < m2.NoRows() {
					res[y][x] += m1[y][x] + m2[smallerMatrixY][smallerMatrixX]
					continue
				}
				res[y][x] += m1[y][x]
			}
		}
	}
	return res
}
