package matrices

import (
	"testing"
)

type testCaseAddToAll struct {
	filepath    string
	add         int
	rowToTest   int
	expected    []int
	description string
}

var addToAll = []testCaseAddToAll{
	{
		filepath:    "./test_data/simple_matrix.txt",
		add:         2,
		rowToTest:   0,
		expected:    []int{3, 3, 3, 3, 3},
		description: "example",
	},

	{
		filepath:    "./test_data/simple_matrix.txt",
		add:         10,
		rowToTest:   1,
		expected:    []int{11, 19, 19, 19, 11},
		description: "example",
	},
}

func TestAddToAll(t *testing.T) {
	for _, tc := range addToAll {
		m := Matrix{}
		m.NewDigitsFromFile(tc.filepath)
		m.AddToAll(tc.add)
		actual := m[tc.rowToTest]
		if !Equal(actual, tc.expected) {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

type testCaseCountAbove struct {
	filepath    string
	threshold   int
	expected    int
	description string
}

var CountAbove = []testCaseCountAbove{
	{
		filepath:    "./test_data/simple_matrix.txt",
		threshold:   5,
		expected:    8,
		description: "example",
	},

	{
		filepath:    "./test_data/simple_matrix.txt",
		threshold:   9,
		expected:    0,
		description: "example",
	},

	{
		filepath:    "./test_data/simple_matrix.txt",
		threshold:   8,
		expected:    8,
		description: "example",
	},
}

func TestCountAbove(t *testing.T) {
	for _, tc := range CountAbove {
		m := Matrix{}
		m.NewDigitsFromFile(tc.filepath)
		actual := m.CountAbove(tc.threshold)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
