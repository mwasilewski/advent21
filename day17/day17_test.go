package day17

import "testing"

func TestFindHighest(t *testing.T) {
	tcs := []struct {
		filepath    string
		expected    int
		description string
	}{
		{filepath: "./test_data/input1", expected: 45, description: "example"},
		{filepath: "./test_data/input2", expected: 10296, description: "puzzle"},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			actual := FindHighest(tc.filepath)
			if actual != tc.expected {
				t.Errorf("expected: %d, got: %d", tc.expected, actual)
			}
		})
	}
}

func TestCount(t *testing.T) {
	tcs := []struct {
		filepath    string
		expected    int
		description string
	}{
		{filepath: "./test_data/input1", expected: 112, description: "example"},
		{filepath: "./test_data/input2", expected: 2371, description: "puzzle"},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			actual := Count(tc.filepath)
			if actual != tc.expected {
				t.Errorf("expected: %d, got: %d", tc.expected, actual)
			}
		})
	}
}
