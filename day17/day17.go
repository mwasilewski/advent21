package day17

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type coordinates struct {
	X int
	Y int
}

type probe struct {
	velocity coordinates
	pos      coordinates
}

func (p *probe) step() {
	p.pos.X += p.velocity.X
	p.pos.Y += p.velocity.Y

	// x drag
	if p.velocity.X < 0 {
		p.velocity.X += 1
	} else if p.velocity.X > 0 {
		p.velocity.X -= 1
	}

	// y drag
	p.velocity.Y -= 1
}

type area struct {
	upperLeft  coordinates
	lowerRight coordinates
}

func (a area) inArea(c coordinates) bool {
	if a.upperLeft.X <= c.X && c.X <= a.lowerRight.X && a.upperLeft.Y >= c.Y && a.lowerRight.Y <= c.Y {
		return true
	}
	return false
}

func (a area) isOvershot(c coordinates) bool {
	if c.X > a.lowerRight.X || c.Y < a.lowerRight.Y {
		return true
	}
	return false
}

func conv(s string) int {
	res, err := strconv.Atoi(s)
	if err != nil {
		log.Println(fmt.Errorf("error converting string to integer: %w", err))
	}
	return res
}

func loadInput(filepath string) *area {
	// load file into a string
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open file: %w", err))
	}
	defer fd.Close()
	scanner := bufio.NewScanner(fd)
	scanner.Scan() // expect a single line in input
	line := scanner.Text()

	// parse string
	r, _ := regexp.Compile("[x,y]=-?\\d+\\.\\.-?\\d+")
	matches := r.FindAllString(line, -1)
	rx := []rune(matches[0])
	ry := []rune(matches[1])
	x := strings.Split(string(rx[2:]), "..")
	y := strings.Split(string(ry[2:]), "..")

	x1 := conv(x[0])
	x2 := conv(x[1])
	y1 := conv(y[0])
	y2 := conv(y[1])

	if x1 > x2 {
		x1, x2 = x2, x1
	}
	if y1 < y2 {
		y1, y2 = y2, y1
	}

	// return a struct
	a1 := &area{}
	a1.upperLeft.X = x1
	a1.upperLeft.Y = y1
	a1.lowerRight.X = x2
	a1.lowerRight.Y = y2
	return a1
}

type simulation struct {
	initialVelocity coordinates
	path            []coordinates
}

func generatePossibleVelocities(a *area) []coordinates {
	maxX := a.lowerRight.X
	minY := a.lowerRight.Y

	possibleVelocities := []coordinates{}
	for x := 0; x < maxX+1; x++ {
		for y := minY; y < maxX*2; y++ {
			possibleVelocities = append(possibleVelocities, coordinates{X: x, Y: y})
		}
	}
	return possibleVelocities

}

func simulateLaunch(a *area) []*simulation {
	res := []*simulation{}
	possibleVelocities := generatePossibleVelocities(a)
	for _, vel := range possibleVelocities {
		sim := simulation{initialVelocity: vel, path: nil}
		p := probe{velocity: vel, pos: coordinates{X: 0, Y: 0}}
		sim.path = append(sim.path, p.pos)
		for {
			p.step()
			sim.path = append(sim.path, p.pos)
			if a.inArea(p.pos) {
				res = append(res, &sim)
				break
			}
			if a.isOvershot(p.pos) {
				break
			}
		}
	}
	return res
}

func findHighest(simulations []*simulation) int {
	var maxY int
	for _, sim := range simulations {
		for _, point := range sim.path {
			if point.Y > maxY {
				maxY = point.Y
			}
		}
	}
	return maxY
}

func FindHighest(filepath string) int {
	targetArea := loadInput(filepath)
	hittingSimulations := simulateLaunch(targetArea)
	maxY := findHighest(hittingSimulations)
	return maxY
}

func Count(filepath string) int {
	targetArea := loadInput(filepath)
	hittingSimulations := simulateLaunch(targetArea)
	return len(hittingSimulations)
}
