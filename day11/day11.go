package day11

import (
	"fmt"
	"gitlab.com/mwasilewski/advent21/matrices"
	"log"
)

const (
	energyIncStep     = 1
	energyIncFlashing = 1
	flashingThreshold = 9
	startingLevel     = 0
)

var directions = []func(matrix matrices.Matrix, p *matrices.Point) (*matrices.Point, error){
	matrices.Matrix.Up,
	matrices.Matrix.UpperRight,
	matrices.Matrix.Right,
	matrices.Matrix.LowerRight,
	matrices.Matrix.Down,
	matrices.Matrix.UpperLeft,
	matrices.Matrix.Left,
	matrices.Matrix.LowerLeft,
}

type octopusMap struct {
	m            matrices.Matrix
	nextFlashing *matrices.Point
}

func (om *octopusMap) findFlashing(threshold int) bool {
	for y := 0; y < len(om.m); y++ {
		for x := 0; x < len(om.m[0]); x++ {
			if om.m[y][x] > threshold {
				om.nextFlashing = &matrices.Point{
					X: x,
					Y: y,
				}
				return true
			}
		}
	}
	return false
}

func increaseNeighbors(om *octopusMap, p *matrices.Point, step int) {
	var neighbors []*matrices.Point
	for _, direction := range directions {
		neighbor, err := direction(om.m, p)
		if err != nil {
			log.Println(fmt.Errorf("failed to get neighbor: %w", err))
			continue
		}
		neighbors = append(neighbors, neighbor)
	}
	for _, neighbor := range neighbors {
		if om.m[neighbor.Y][neighbor.X] == 0 { // already flashed
			continue
		}
		om.m[neighbor.Y][neighbor.X] += step
	}

}

func stepAndFlash(m matrices.Matrix) int {
	var cnt int
	m.AddToAll(energyIncStep)
	om := octopusMap{
		m: m,
	}
	for om.findFlashing(flashingThreshold) {
		increaseNeighbors(&om, om.nextFlashing, energyIncFlashing)
		om.m[om.nextFlashing.Y][om.nextFlashing.X] = startingLevel
		om.nextFlashing = nil
		cnt += 1
	}
	return cnt
}

func modelFlashes(filepath string, noSteps int) int {
	var totalFlashes int

	m := matrices.Matrix{}
	m.NewDigitsFromFile(filepath)
	for i := 1; i < noSteps+1; i++ {
		noFlashes := stepAndFlash(m)
		totalFlashes += noFlashes
	}
	return totalFlashes
}

func findSimultaneous(filepath string) int {
	var cnt int
	m := matrices.Matrix{}
	m.NewDigitsFromFile(filepath)
	for noFlashes := 0; noFlashes < 100; {
		noFlashes = stepAndFlash(m)
		cnt += 1
	}
	return cnt
}
