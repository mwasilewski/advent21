package day11

import "testing"

type testCase struct {
	filepath    string
	noSteps     int
	expected    int
	description string
}

var countDigitsTestCases = []testCase{
	{
		filepath:    "./test_data/input0",
		noSteps:     2,
		expected:    9,
		description: "example",
	},

	{
		filepath:    "./test_data/input1",
		noSteps:     2,
		expected:    35,
		description: "example",
	},

	{
		filepath:    "./test_data/input1",
		noSteps:     100,
		expected:    1656,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		noSteps:     100,
		expected:    1620,
		description: "actual puzzle",
	},
}

func TestModelFlashes(t *testing.T) {
	for _, tc := range countDigitsTestCases {
		actual := modelFlashes(tc.filepath, tc.noSteps)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

var TCsFindSimultaneous = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    195,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    371,
		description: "actual puzzle",
	},
}

func TestFindSimultaneous(t *testing.T) {
	for _, tc := range TCsFindSimultaneous {
		actual := findSimultaneous(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
