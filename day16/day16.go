package day16

import (
	"bufio"
	"encoding/hex"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	bufferSizeHexReader = 1

	statePacketVersion     = iota
	stateTypeId            = iota
	stateLiteralValue      = iota
	stateOperator          = iota
	stateTotalLengthInBits = iota
	stateNumOfSubPackets   = iota

	sizePacketVersion     = 3
	sizeTypeId            = 3
	sizeLiteralValueGroup = 5
	sizeLenTypeID         = 1
	sizeTotalLenInBits    = 15
	sizeNumOfSubPackets   = 11
)

type Packet struct {
	fsmState     int
	version      int
	typeID       int
	literalValue int
	lenTypeID    int
	subPacketLen int
	noSubPackets int
	innerPackets []*Packet
}

func parsePacket(stream io.Reader) []*Packet {
	packet := &Packet{
		fsmState:     statePacketVersion,
		version:      0,
		typeID:       0,
		literalValue: 0,
		lenTypeID:    0,
		subPacketLen: 0,
		noSubPackets: 0,
		innerPackets: nil,
	}
	for {
		switch packet.fsmState {
		case statePacketVersion:
			buf := make([]byte, sizePacketVersion)
			n, err := stream.Read(buf)
			if err != nil {
				if err == io.EOF {
					return nil
				}
				log.Println(fmt.Errorf("error reading stream: %w", err))
				return nil
			}
			if n < sizePacketVersion {
				return nil
			}
			i, err := strconv.ParseInt(string(buf), 2, 32)
			if err != nil {
				log.Println(fmt.Errorf("failed to convert packet version (string with binary to decimal int): %w", err))
				return nil
			}
			packet.version = int(i)
			packet.fsmState = stateTypeId
		case stateTypeId:
			buf := make([]byte, sizeTypeId)
			n, err := stream.Read(buf)
			if n < sizeTypeId {
				return nil
			}
			if err != nil {
				log.Println(fmt.Errorf("error reading stream: %w", err))
			}
			i, err := strconv.ParseInt(string(buf), 2, 32)
			if err != nil {
				log.Println(fmt.Errorf("failed to convert typeID (string with binary to decimal int): %w", err))
				return nil
			}
			packet.typeID = int(i)
			if packet.typeID == 4 {
				packet.fsmState = stateLiteralValue
				continue
			}
			packet.fsmState = stateOperator
		case stateLiteralValue:
			buf := make([]byte, sizeLiteralValueGroup)
			value := strings.Builder{}

			for {
				_, err := stream.Read(buf)
				if err != nil {
					log.Println(fmt.Errorf("error reading stream: %w", err))
				}
				if buf[0] == '0' {
					value.Write(buf[1:])
					break
				}
				value.Write(buf[1:])
			}
			i, err := strconv.ParseInt(value.String(), 2, 64)
			if err != nil {
				log.Println(fmt.Errorf("failed to convert literal value (string with binary to decimal int): %w", err))
				return nil
			}
			packet.literalValue = int(i)
			result := []*Packet{packet}
			return result
		case stateOperator:
			buf := make([]byte, sizeLenTypeID)
			_, err := stream.Read(buf)
			if err != nil {
				log.Println(fmt.Errorf("error reading stream: %w", err))
			}
			i, err := strconv.ParseInt(string(buf), 2, 64)
			if err != nil {
				log.Println(fmt.Errorf("failed to convert literal value (string with binary to decimal int): %w", err))
				return nil
			}
			packet.lenTypeID = int(i)
			if packet.lenTypeID == 0 {
				packet.fsmState = stateTotalLengthInBits
				continue
			}
			packet.fsmState = stateNumOfSubPackets
		case stateTotalLengthInBits:
			buf := make([]byte, sizeTotalLenInBits)
			_, err := stream.Read(buf)
			if err != nil {
				log.Println(fmt.Errorf("error reading stream: %w", err))
			}
			i, err := strconv.ParseInt(string(buf), 2, 64)
			if err != nil {
				log.Println(fmt.Errorf("failed to convert literal value (string with binary to decimal int): %w", err))
				return nil
			}
			packet.subPacketLen = int(i)

			// create a reader for the remaining part of the transmission
			bufSubpackets := make([]byte, packet.subPacketLen)
			_, err = stream.Read(bufSubpackets)
			if err != nil {
				log.Println(fmt.Errorf("error reading stream: %w", err))
			}
			rest := string(bufSubpackets)
			streamNested := strings.NewReader(rest)
			for {
				innerPackets := parsePacket(streamNested)
				if innerPackets == nil {
					break
				}
				packet.innerPackets = append(packet.innerPackets, innerPackets...)
			}
			return []*Packet{packet}
		case stateNumOfSubPackets:
			buf := make([]byte, sizeNumOfSubPackets)
			_, err := stream.Read(buf)
			if err != nil {
				log.Println(fmt.Errorf("error reading stream: %w", err))
			}
			i, err := strconv.ParseInt(string(buf), 2, 64)
			if err != nil {
				log.Println(fmt.Errorf("failed to convert literal value (string with binary to decimal int): %w", err))
				return nil
			}
			packet.noSubPackets = int(i)
			for j := 0; j < packet.noSubPackets; j++ {
				packet.innerPackets = append(packet.innerPackets, parsePacket(stream)...)
			}
			return []*Packet{packet}

		}
	}
	log.Println("should never be reached")
	return []*Packet{packet}
}

func sumOfVersions(packets []*Packet) int {
	var sum int
	for _, packet := range packets {
		sum += packet.version
		if packet.innerPackets != nil {
			sum += sumOfVersions(packet.innerPackets)
		}

	}
	return sum
}

func decodeTransmission(filepath string) string {
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("error opening file: %w", err))
	}
	defer fd.Close()

	bfd := bufio.NewReader(fd)

	//transmission := bytes.NewBuffer([]byte{})
	transmission := strings.Builder{}
	var decoderBuffer byte

decoderLoop:
	for {
		decoderBuffer, err = bfd.ReadByte()
		if err != nil {
			switch err {
			case io.EOF:
				break decoderLoop
			default:
				log.Println(fmt.Errorf("error reading buffered input: %w", err))
				break decoderLoop
			}
		}
		bytesBuffer, err := hex.DecodeString("0" + string(decoderBuffer))
		if err != nil {
			log.Println(fmt.Errorf("failed to decode hex: %w", err))
			panic("failed to decode hex")
		}

		transmission.WriteString(fmt.Sprintf("%04b", bytesBuffer[0]))

	}

	transmissionS := transmission.String()
	return transmissionS
}

func versionsSum(filepath string) int {
	transmission := decodeTransmission(filepath)
	packets := parsePacket(strings.NewReader(transmission))
	sum := sumOfVersions(packets)

	return sum
}

func evaluateExpression(packets []*Packet) int {
	var value int
	for _, packet := range packets {
		switch packet.typeID {
		case 0:
			for _, subPacket := range packet.innerPackets {
				value += evaluateExpression([]*Packet{subPacket})
			}
		case 1:
			value = 1
			for _, subPacket := range packet.innerPackets {
				value = value * evaluateExpression([]*Packet{subPacket})
			}
		case 2:
			isFirst := true
			for _, subPacket := range packet.innerPackets {
				subPacketValue := evaluateExpression([]*Packet{subPacket})
				if isFirst {
					value = subPacketValue
					isFirst = false
				} else if subPacketValue < value {
					value = subPacketValue
				}
			}
		case 3:
			isFirst := true
			for _, subPacket := range packet.innerPackets {
				subPacketValue := evaluateExpression([]*Packet{subPacket})
				if isFirst {
					value = subPacketValue
					isFirst = false
				} else if subPacketValue > value {
					value = subPacketValue
				}
			}
		case 4:
			value += packet.literalValue
		case 5:
			value1 := evaluateExpression([]*Packet{packet.innerPackets[0]})
			value2 := evaluateExpression([]*Packet{packet.innerPackets[1]})
			if value1 > value2 {
				value = 1
				continue
			}
			value = 0
		case 6:
			value1 := evaluateExpression([]*Packet{packet.innerPackets[0]})
			value2 := evaluateExpression([]*Packet{packet.innerPackets[1]})
			if value1 < value2 {
				value = 1
				continue
			}
			value = 0
		case 7:
			value1 := evaluateExpression([]*Packet{packet.innerPackets[0]})
			value2 := evaluateExpression([]*Packet{packet.innerPackets[1]})
			if value1 == value2 {
				value = 1
				continue
			}
			value = 0
		}
	}
	return value
}

func evaluateTransmission(filepath string) int {
	transmission := decodeTransmission(filepath)
	packets := parsePacket(strings.NewReader(transmission))
	value := evaluateExpression(packets)
	return value
}
