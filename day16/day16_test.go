package day16

import "testing"

func TestVersionsSum(t *testing.T) {
	tcs := []struct {
		filepath    string
		expected    int
		description string
	}{
		{filepath: "./test_data/input00", expected: 6, description: "simple literal value"},
		{filepath: "./test_data/input000", expected: 9, description: "inner packets"},
		{filepath: "./test_data/input0000", expected: 14, description: "subpackets"},
		{filepath: "./test_data/input01", expected: 16, description: "first example"},
		{filepath: "./test_data/input02", expected: 12, description: "second example"},
		{filepath: "./test_data/input03", expected: 23, description: "third example"},
		{filepath: "./test_data/input04", expected: 31, description: "fourth example"},
		{filepath: "./test_data/input2", expected: 971, description: "puzzle"},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			actual := versionsSum(tc.filepath)
			if actual != tc.expected {
				t.Errorf("expected: %d, got : %d", tc.expected, actual)
			}
		})
	}
}

func TestEvaluateTransmission(t *testing.T) {
	tcs := []struct {
		filepath    string
		expected    int
		description string
	}{
		{filepath: "./test_data/part2/input1", expected: 3, description: "first ex"},
		{filepath: "./test_data/part2/input2", expected: 54, description: "second ex"},
		{filepath: "./test_data/part2/input3", expected: 7, description: "third ex"},
		{filepath: "./test_data/part2/input4", expected: 9, description: "fourth ex"},
		{filepath: "./test_data/part2/input5", expected: 1, description: "fifth ex"},
		{filepath: "./test_data/part2/input6", expected: 0, description: "sixth ex"},
		{filepath: "./test_data/part2/input7", expected: 0, description: "seventh ex"},
		{filepath: "./test_data/part2/input8", expected: 1, description: "eighth ex"},
		{filepath: "./test_data/part2/input_puzzle", expected: 831996589851, description: "puzzle"},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			actual := evaluateTransmission(tc.filepath)
			if actual != tc.expected {
				t.Errorf("expected: %d, got: %d", tc.expected, actual)
			}
		})
	}
}
