package day10

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"sort"
	"sync"
)

var (
	openingBrackets = [4]rune{'(', '[', '{', '<'}
	closingBrackets = [4]rune{')', ']', '}', '>'}

	matchingClosing = map[rune]rune{
		'(': ')',
		'[': ']',
		'{': '}',
		'<': '>',
	}

	ptsErrorScore = map[rune]int{
		')': 3,
		']': 57,
		'}': 1197,
		'>': 25137,
	}

	ptsCompletion = map[rune]int{
		')': 1,
		']': 2,
		'}': 3,
		'>': 4,
	}
)

type stack struct {
	lock sync.Mutex
	s    []rune
}

func NewStack() *stack {
	return &stack{sync.Mutex{}, make([]rune, 0)}
}

func (s *stack) Push(v rune) {
	s.lock.Lock()
	defer s.lock.Unlock()

	s.s = append(s.s, v)
}

func (s *stack) Pop() (rune, error) {
	s.lock.Lock()
	defer s.lock.Unlock()

	l := len(s.s)
	if l == 0 {
		return 0, errors.New("Empty Stack")
	}

	res := s.s[l-1]
	s.s = s.s[:l-1]
	return res, nil
}

func isClosing(r rune) bool {
	for _, opening := range openingBrackets {
		if r == opening {
			return false
		}
	}
	return true
}

func isOpening(r rune) bool {
	for _, closing := range closingBrackets {
		if r == closing {
			return false
		}
	}
	return true
}

func lineScore(s string) (int, error) {
	st := NewStack()
	for _, r := range s {
		if isOpening(r) {
			st.Push(r)
			continue
		}
		if isClosing(r) {
			opening, err := st.Pop()
			if err != nil {
				return 0, fmt.Errorf("failed to pop an item off the stack: %w", err)
			}
			if matchingClosing[opening] != r {
				return ptsErrorScore[r], nil
			}
			continue
		}
	}
	return 0, nil
}

func syntaxErrorScore(filepath string) int {
	var sum int
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open file: %w", err))
	}
	defer fd.Close()

	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		line := scanner.Text()
		score, err := lineScore(line)
		if err != nil {
			log.Println(fmt.Errorf("error getting line score: %w", err))
			continue
		}
		sum += score
	}
	if err := scanner.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan the file"))
	}

	return sum
}

func closingStringScore(s string) int {
	var score int
	for _, r := range s {
		score = score * 5
		score += ptsCompletion[r]
	}
	return score

}

func genClosingString(s string) (string, error) {
	st := NewStack()
	for _, r := range s {
		if isOpening(r) {
			st.Push(r)
			continue
		}
		if isClosing(r) {
			_, err := st.Pop()
			if err != nil {
				return "", fmt.Errorf("failed to pop an item off the stack for a matching closing bracket: %w", err)
			}
			continue
		}
	}
	var closingString []rune
	if len(st.s) != 0 { // unmatched brackets left on the stack
		for len(st.s) != 0 { // for each item left on the stack
			unmatchedOpening, err := st.Pop()
			if err != nil {
				return "", fmt.Errorf("failed to pop an item off the stack for the closing string: %w", err)
			}
			closingString = append(closingString, matchingClosing[unmatchedOpening])
		}
	}
	return string(closingString), nil
}

func middleCompletionScore(filepath string) int {
	// open file
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open file: %w", err))
	}
	defer fd.Close()

	// find incomplete lines
	var incompleteLines []string
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		line := scanner.Text()
		score, err := lineScore(line)
		if err != nil {
			log.Println(fmt.Errorf("error getting line score: %w", err))
			continue
		}
		if score == 0 {
			incompleteLines = append(incompleteLines, line)
		}
	}
	if err := scanner.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan the file"))
	}

	// generate closing strings
	var closingStrings []string
	for _, incompleteLine := range incompleteLines {
		closingString, err := genClosingString(incompleteLine)
		if err != nil {
			log.Println(fmt.Errorf("failed to get a closing string: %w", err))
		}
		closingStrings = append(closingStrings, closingString)
	}

	// score completion strings
	var scores []int
	for _, closingString := range closingStrings {
		score := closingStringScore(closingString)
		scores = append(scores, score)
	}

	// return result
	sort.Ints(scores)
	midInd := len(scores)/2 + 1 - 1
	return scores[midInd]
}
