package day10

import "testing"

type testCase struct {
	filepath    string
	expected    int
	description string
}

var syntaxErrorScoreTestCases = []testCase{
	{
		filepath:    "./test_data/input0",
		expected:    0,
		description: "simplest case",
	},

	{
		filepath:    "./test_data/input1",
		expected:    26397,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    358737,
		description: "actual puzzle",
	},
}

func TestSyntaxErrorScore(t *testing.T) {
	for _, tc := range syntaxErrorScoreTestCases {
		actual := syntaxErrorScore(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

var middleCompletionScoreTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    288957,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    4329504793,
		description: "actual puzzle",
	},
}

func TestMiddleCompletionScore(t *testing.T) {
	for _, tc := range middleCompletionScoreTestCases {
		actual := middleCompletionScore(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
