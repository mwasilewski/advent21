package day13

import (
	"bufio"
	"fmt"
	"gitlab.com/mwasilewski/advent21/matrices"
	"log"
	"os"
	"strconv"
	"strings"
)

const (
	fieldValueEmpty = 0
	fieldValueDot   = 1
)

type command struct {
	dimension string
	value     int
}

func fold(m *matrices.Matrix, c *command) *matrices.Matrix {
	var s1UpperLeft matrices.Point
	var s1LowerRight matrices.Point
	var s2UpperLeft matrices.Point
	var s2LowerRight matrices.Point

	// get edge points of submatrices
	switch c.dimension {
	case "x":
		// left submatrix, s1
		s1UpperLeft.X = 0
		s1UpperLeft.Y = 0
		s1LowerRight.X = c.value - 1
		s1LowerRight.Y = m.NoRows() - 1
		// right submatrix, s2
		s2UpperLeft.X = c.value + 1
		s2UpperLeft.Y = 0
		s2LowerRight.X = m.NoCols() - 1
		s2LowerRight.Y = m.NoRows() - 1
	case "y":
		// upper submatrix, s1
		s1UpperLeft.X = 0
		s1UpperLeft.Y = 0
		s1LowerRight.X = m.NoCols() - 1
		s1LowerRight.Y = c.value - 1
		// lower submatrix, s2
		s2UpperLeft.X = 0
		s2UpperLeft.Y = c.value + 1
		s2LowerRight.X = m.NoCols() - 1
		s2LowerRight.Y = m.NoRows() - 1
	}

	// get submatrices
	s1 := m.Submatrix(&s1UpperLeft, &s1LowerRight)
	s2 := m.Submatrix(&s2UpperLeft, &s2LowerRight)

	// mirror one matrix
	offset := matrices.Point{}
	switch c.dimension {
	case "x":
		s2 = s2.MirrorVertically()
		offset.X = len(s1[0]) - len(s2[0])
		offset.Y = 0
	case "y":
		s2 = s2.MirrorHorizontally()
		offset.X = 0
		offset.Y = len(s1) - len(s2)
	}

	// add matrices
	result := matrices.AddMatrices(s1, s2, &offset)
	result.Trim(fieldValueDot, fieldValueDot) // dots overlapping

	return &result
}

func loadFromFile(filepath string) (matrices.Matrix, []*command) {
	var m matrices.Matrix
	var commands []*command

	// open file
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open a file: %w", err))
	}
	defer fd.Close()

	// extract data from the file
	var points []*matrices.Point
	var reachedCommands bool
	scannerLines := bufio.NewScanner(fd)
	for scannerLines.Scan() {
		line := scannerLines.Text()
		if line == "" {
			reachedCommands = true
			continue
		}
		if reachedCommands {
			fields := strings.Fields(line)
			lastWord := fields[len(fields)-1]
			l := strings.Split(lastWord, "=")
			dimension := l[0]
			value, err := strconv.Atoi(l[1])
			if err != nil {
				log.Println(fmt.Errorf("failed to convert string to integer at commands: %w", err))
			}
			c := command{
				dimension: dimension,
				value:     value,
			}
			commands = append(commands, &c)
			continue
		}
		fields := strings.Split(line, ",")
		var err1 error
		var x, y int
		x, err1 = strconv.Atoi(fields[0])
		y, err1 = strconv.Atoi(fields[1])
		if err1 != nil {
			log.Println(fmt.Errorf("failed to convert string to integer at points: %w", err1))
		}
		p := matrices.Point{
			X: x,
			Y: y,
		}
		points = append(points, &p)
	}
	if err := scannerLines.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan lines: %w", err))
	}

	// find dimensions to be able to allocate memory for the matrix
	var maxX, maxY int
	for _, point := range points {
		if point.X > maxX {
			maxX = point.X
		}
		if point.Y > maxY {
			maxY = point.Y
		}
	}

	// allocate memory for the matrix
	for i := 0; i < maxY+1; i++ {
		row := make([]int, maxX+1)
		m = append(m, row)
	}

	// populate matrix with points
	for _, point := range points {
		m[point.Y][point.X] = fieldValueDot
	}

	return m, commands
}

func noDots(filepath string, noFolds int) int {
	m, commands := loadFromFile(filepath)
	mpoint := &m
	for i := 0; i < noFolds; i++ {
		mpoint = fold(mpoint, commands[i])
	}
	noDotsInt := mpoint.CountAbove(fieldValueEmpty)
	return noDotsInt
}

func printDots(filepath string) {
	m, commands := loadFromFile(filepath)
	mpoint := &m
	for i := 0; i < len(commands); i++ {
		mpoint = fold(mpoint, commands[i])
	}
	for _, row := range *mpoint {
		fmt.Println(row)
	}
}
