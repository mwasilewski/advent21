package day13

import "testing"

type testCase struct {
	filepath    string
	noFolds     int
	expected    int
	description string
}

var TCsNoDots = []testCase{
	{
		filepath:    "./test_data/input1",
		noFolds:     1,
		expected:    17,
		description: "example",
	},

	{
		filepath:    "./test_data/input1",
		noFolds:     2,
		expected:    16,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		noFolds:     1,
		expected:    661,
		description: "puzzle",
	},
}

func TestNoDots(t *testing.T) {
	for _, tc := range TCsNoDots {
		actual := noDots(tc.filepath, tc.noFolds)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

var TCsPrintDots = []testCase{
	{
		filepath:    "./test_data/input2",
		description: "puzzle",
	},
}

func TestPrintDots(t *testing.T) {
	for _, tc := range TCsPrintDots {
		printDots(tc.filepath)
	}
}
