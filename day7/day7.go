package day7

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"strconv"
)

type crab struct {
	startPos int
}

// dist returns an absolute value of distance between starting point and given dest
func (c crab) dist(dest int) int {
	dist := c.startPos - dest
	if dist < 0 {
		dist = -dist
	}
	return dist
}

// costConst returns the amount of fuel used by a crab to move to a dest, the cost is modeled using a constant function (1 field, 1 unit of fuel)
func (c crab) costConst(dest int) int {
	return c.dist(dest)
}

// costLin returns the amount of fuel used by a crab to move to a dest, the cost is modeled using a linear function (moving from n to n+1 field costs n+1 amount of fuel)
func (c crab) costLin(dest int) int {
	dist := c.dist(dest)
	return sumOfNNatur(dist)
}

// sumOfNNatur returns the sum of n natural numbers
func sumOfNNatur(n int) int {
	return (n * (n + 1)) / 2
}

// SplitAt splits a slice of bytes on a given substring
func SplitAt(substring string) func(data []byte, atEOF bool) (advance int, token []byte, err error) {
	searchBytes := []byte(substring)
	searchLen := len(searchBytes)
	return func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		dataLen := len(data)

		// Return nothing if at end of file and no data passed
		if atEOF && dataLen == 0 {
			return 0, nil, nil
		}

		// Find next separator and return token
		if i := bytes.Index(data, searchBytes); i >= 0 {
			return i + searchLen, data[0:i], nil
		}

		// If we're at EOF, we have a final, non-terminated line. Return it.
		if atEOF {
			return dataLen, data, nil
		}

		// Request more data.
		return 0, nil, nil
	}
}

// loadData loads input data for this given day
func loadData(filepath string) []crab {
	var crabs []crab
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open file: %w", err))
	}
	defer fd.Close()

	line := bufio.NewScanner(fd)
	line.Scan()
	r := bytes.NewReader(line.Bytes())
	scannerCrabs := bufio.NewScanner(r)
	scannerCrabs.Split(SplitAt(","))
	for scannerCrabs.Scan() {
		sPos := scannerCrabs.Text()
		iPos, err := strconv.Atoi(sPos)
		if err != nil {
			log.Println(fmt.Errorf("failed to convert string to int: %w", err))
		}
		crabs = append(
			crabs,
			crab{
				startPos: iPos,
			},
		)
	}
	return crabs
}

// find min value in a map[int]int
func findMin(simulations map[int]int) int {
	min := simulations[0]
	for _, sim := range simulations {
		if sim < min {
			min = sim
		}
	}
	return min
}

// simulate crab movement using given cost function
func simulate(crabs []crab, costFunc func(c crab, dest int) int) map[int]int {
	simulations := make(map[int]int) // dest:fuel
	for dest := 0; dest < len(crabs); dest++ {
		var totalFuelUsed int
		for _, c := range crabs {
			fuelUsed := costFunc(c, dest)
			totalFuelUsed += fuelUsed
		}
		simulations[dest] = totalFuelUsed
	}
	return simulations
}

// crabAlign returns the optimal position for aligning crabs using the given cost function
func crabAlign(filepath string, costFunc func(c crab, dest int) int) int {
	crabs := loadData(filepath)
	simulations := simulate(crabs, costFunc)
	min := findMin(simulations)
	return min
}
