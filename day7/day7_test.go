package day7

import "testing"

type testCase struct {
	filepath    string
	expected    int
	description string
}

var crabAlignConstTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    37,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    352997,
		description: "actual puzzle",
	},
}

func TestCrabAlignConst(t *testing.T) {
	for _, tc := range crabAlignConstTestCases {
		actual := crabAlign(tc.filepath, crab.costConst)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

var crabAlignLinTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    168,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    101571302,
		description: "actual puzzle",
	},
}

func TestCrabAlignLinConst(t *testing.T) {
	for _, tc := range crabAlignLinTestCases {
		actual := crabAlign(tc.filepath, crab.costLin)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
