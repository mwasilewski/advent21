package day1

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

//numOfIncreases returns number of times depth measurement in the given file increases
func numOfIncreases(filepath string) (int, error) {
	var (
		fd  *os.File
		err error
	)
	fd, err = os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open file: %w", err))
		return 0, err
	}
	defer fd.Close()

	var (
		start     = true
		previous  int
		current   int
		increases int
	)
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		current, err = strconv.Atoi(scanner.Text())
		if err != nil {
			log.Println(fmt.Errorf("failed to convert text to integer: %w", err))
			return 0, err
		}
		if start {
			start = false
			previous = current
			continue
		}
		if previous >= current {
			previous = current
			continue
		}
		increases += 1
		previous = current
	}
	if err := scanner.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan the file: %w", err))
		return 0, err
	}
	return increases, nil
}

//numOfIncreasesWin returns number of times depth measurements increased when considering a measurement window
func numOfIncreasesWin(filepath string, winLen int) (int, error) {
	var (
		fd  *os.File
		err error
	)
	fd, err = os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open file: %w", err))
		return 0, err
	}
	defer fd.Close()

	var (
		current   int
		increases int
		fifo      []int
	)
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		current, err = strconv.Atoi(scanner.Text())
		if err != nil {
			log.Println(fmt.Errorf("failed to convert text to integer: %w", err))
			return 0, err
		}

		fifo = append(fifo, current)
		if len(fifo) < winLen+1 {
			continue
		}
		if sum(fifo[:3]) >= sum(fifo[1:]) {
			fifo = fifo[1:]
			continue
		}
		increases += 1
		fifo = fifo[1:]
	}
	if err := scanner.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan the file: %w", err))
		return 0, err
	}
	return increases, nil
}

//sum function returns the sum of elements in an integer slice
func sum(s []int) int {
	var sum int
	for _, v := range s {
		sum += v
	}
	return sum
}
