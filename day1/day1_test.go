package day1

import (
	"testing"
)

func TestNumOfIncreases(t *testing.T) {
	type testCase struct {
		filepath    string
		expectedInc int
		expectedErr bool
		description string
	}

	testcases := []testCase{
		{
			filepath:    "./test_data/measure0",
			expectedInc: 7,
			expectedErr: false,
			description: "example",
		},
		{
			filepath:    "./test_data/measure1",
			expectedInc: 1342,
			expectedErr: false,
			description: "actual problem",
		},
	}

	for _, tc := range testcases {
		actual, err := numOfIncreases(tc.filepath)
		if actual != tc.expectedInc {
			t.Errorf("Expected number of increases: %d, got: %d", tc.expectedInc, actual)
		}
		if (err != nil) != tc.expectedErr {
			t.Errorf("got an unexpected error: %s", err)
		}
	}
}
func TestNumOfIncreasesWin(t *testing.T) {
	type testCase struct {
		filepath    string
		winLen      int
		expectedInc int
		expectedErr bool
		description string
	}

	testcases := []testCase{
		{
			filepath:    "./test_data/measure0",
			winLen:      3,
			expectedInc: 5,
			expectedErr: false,
			description: "example",
		},
		{
			filepath:    "./test_data/measure1",
			winLen:      3,
			expectedInc: 1378,
			expectedErr: false,
			description: "actual problem",
		},
	}

	for _, tc := range testcases {
		actual, err := numOfIncreasesWin(tc.filepath, tc.winLen)
		if actual != tc.expectedInc {
			t.Errorf("Expected number of increases: %d, got: %d", tc.expectedInc, actual)
		}
		if (err != nil) != tc.expectedErr {
			t.Errorf("got an unexpected error: %s", err)
		}
	}
}

func TestSum(t *testing.T) {
	type tc struct {
		s        []int
		expected int
	}
	tcs := []tc{
		{
			s:        []int{1, 2, 3, 4},
			expected: 10,
		},
		{
			s:        []int{5, 6, 7, 8},
			expected: 26,
		},
	}
	for _, tc := range tcs {
		actual := sum(tc.s)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
