package day3

import "testing"

type testCase struct {
	filepath    string
	expected    int
	description string
}

var powerTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    198,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    749376,
		description: "actual problem",
	},
}

func TestPower(t *testing.T) {
	for _, tc := range powerTestCases {
		actual, _ := Power(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

var lifeSupportTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    230,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    2372923,
		description: "actual problem",
	},
}

func TestLifeSupport(t *testing.T) {
	for _, tc := range lifeSupportTestCases {
		actual, _ := LifeSupport(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
