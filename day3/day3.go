package day3

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strconv"
	"strings"
)

const (
	zero  = iota
	one   = iota
	equal = iota
)

type equality int

type view map[int][]rune

type report struct {
	rawData []byte
	columns view
	rows    view
}

type Option func(r *report) error

func NewReport(raw []byte, opts ...Option) (*report, error) {
	var (
		err error
	)
	r := report{
		rawData: raw,
	}

	for _, opt := range opts {
		err = opt(&r)
		if err != nil {
			return nil, fmt.Errorf("failed to execute option: %w", err)
		}
	}

	return &r, nil
}

func fillColumns() Option {
	return func(r *report) error {
		cols := make(map[int][]rune)
		scanner := bufio.NewScanner(bytes.NewReader(r.rawData))
		for scanner.Scan() {
			line := []rune(scanner.Text())
			for k, r := range line {
				cols[k] = append(cols[k], r)
			}
		}
		if err := scanner.Err(); err != nil {
			return fmt.Errorf("failed to scan columns: %w", err)
		}
		r.columns = cols
		return nil
	}
}

func fillRows() Option {
	return func(r *report) error {
		rows := make(map[int][]rune)
		scanner := bufio.NewScanner(bytes.NewReader(r.rawData))
		for i := 0; scanner.Scan(); i++ {
			rows[i] = []rune(scanner.Text())
		}
		if err := scanner.Err(); err != nil {
			return fmt.Errorf("failed to scan rows: %w", err)
		}
		r.rows = rows
		return nil
	}
}

func mostCommonBit(slice []rune) equality {
	count0 := strings.Count(string(slice), "0")
	count1 := strings.Count(string(slice), "1")
	switch {
	case count0 > count1:
		return zero
	case count1 > count0:
		return one
	case count0 == count1:
		return equal
	}
	panic(fmt.Errorf("should not be reached"))
}

//Power returns the power consumption of the submarine based on a report contained in a file
func Power(filepath string) (int, error) {
	var err error
	var bs []byte
	bs, err = os.ReadFile(filepath)
	if err != nil {
		return 0, fmt.Errorf("failed to read file: %w", err)
	}

	var rep *report
	rep, err = NewReport(bs, fillColumns())
	if err != nil {
		return 0, fmt.Errorf("failed to create the report: %w", err)
	}

	var (
		gammaRate   uint
		epsilonRate uint
	)
	// maps in go are unordered, that's why a loop with an index is used
	for i := 0; i < len(rep.columns); i++ {
		switch mostCommonBit(rep.columns[i]) {
		case one:
			gammaRate = gammaRate << 1
			gammaRate += 1

			epsilonRate = epsilonRate << 1
		case zero:
			gammaRate = gammaRate << 1

			epsilonRate = epsilonRate << 1
			epsilonRate += 1
		case equal:
			return 0, fmt.Errorf("wrong input")
		default:
			panic("should not be reached")

		}
	}
	return int(epsilonRate * gammaRate), nil
}

func oxygenCheck(rs []rune) rune {
	e := mostCommonBit(rs)
	if e == one || e == equal {
		return '1'
	}
	return '0'
}

func co2check(rs []rune) rune {
	e := mostCommonBit(rs)
	if e == one || e == equal {
		return '0'
	}
	return '1'
}

func genRate(bs []byte, c int, check func([]rune) rune) (int64, error) {
	var err error
	var rep *report
	var next []byte
	rep, err = NewReport(bs, fillColumns(), fillRows())
	if err != nil {
		return 0, fmt.Errorf("failed to create the report: %w", err)
	}
	switch {
	case len(rep.rows) == 1:
		i, err := strconv.ParseInt(string(rep.rows[0]), 2, 64)
		if err != nil {
			return 0, fmt.Errorf("failed to convert string representation of bits into integer")
		}
		return i, nil
	case len(rep.rows) > 1:
		kr := check(rep.columns[c])
		for i := 0; i < len(rep.rows); i++ {
			row := rep.rows[i]
			if row[c] == kr {
				next = append(next, []byte(string(row))...)
				next = append(next, []byte("\n")...)
			}
		}
		return genRate(next, c+1, check)
	default:
		panic("should not be reached, wrong number of nodes")
	}
}

func LifeSupport(filepath string) (int, error) {
	var (
		err error
		bs  []byte
	)
	bs, err = os.ReadFile(filepath)
	if err != nil {
		return 0, fmt.Errorf("failed to read file: %w", err)
	}

	oxygenGenRate, err := genRate(bs, 0, oxygenCheck)
	if err != nil {
		return 0, fmt.Errorf("failed to generate oxygen rate: %w", err)
	}
	co2scrubberRate, err := genRate(bs, 0, co2check)
	if err != nil {
		return 0, fmt.Errorf("failed to generate co2 scrubber rate: %w", err)
	}
	return int(oxygenGenRate * co2scrubberRate), nil
}
