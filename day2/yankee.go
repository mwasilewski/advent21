package day2

import "log"

type subYankee struct {
	horiz int
	depth int
}

func (s *subYankee) Horiz() int {
	return s.horiz
}

func (s *subYankee) Depth() int {
	return s.depth
}

func (s *subYankee) Dive(course []mv) error {
	for _, v := range course {
		switch v.direction {
		case forwString:
			s.horiz += v.distance
		case downString:
			s.depth += v.distance
		case upString:
			s.depth -= v.distance
		default:
			log.Println("Should never be reached")
			panic("should never be reached")
		}
	}
	return nil
}
