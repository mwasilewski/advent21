package day2

import (
	"fmt"
	"log"
)

// class type is used for encoding submarine classes using integers
type class int

const (
	forwString = "forward"
	downString = "down"
	upString   = "up"

	//submarine classes encoding
	Yankee  class = iota
	Typhoon class = iota
)

// mv contains info about a single move done by a submarine
type mv struct {
	direction string
	distance  int
}

type submarine interface {
	Dive(course []mv) error
	Horiz() int
	Depth() int
}

//newSub returns a new submarine
func newSub(subType class) submarine {
	switch subType {
	case Yankee:
		return &subYankee{}
	case Typhoon:
		return &subTyphoon{}
	default:
		panic("should never be reached")
	}
	return nil
}

//Start runs a submarine of a given class on a course defined in a file
func Start(filepath string, subType class) int {
	course, err := marshallCourse(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to marshall course from file: %w", err))
	}
	sub := newSub(subType)
	if err := sub.Dive(course); err != nil {
		log.Println(fmt.Errorf("failed to dive using the given course: %w", err))
	}
	return sub.Horiz() * sub.Depth()
}
