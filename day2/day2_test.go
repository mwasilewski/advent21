package day2

import "testing"

type testCase struct {
	filepath    string
	class       class
	expected    int
	description string
}

var testcases = []testCase{
	{
		filepath:    "./test_data/input1",
		class:       Yankee,
		expected:    150,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		class:       Yankee,
		expected:    2187380,
		description: "actual problem",
	},

	{
		filepath:    "./test_data/input1",
		class:       Typhoon,
		expected:    900,
		description: "part2 example",
	},

	{
		filepath:    "./test_data/input2",
		class:       Typhoon,
		expected:    2086357770,
		description: "part2 actual problem",
	},
}

func TestStart(t *testing.T) {
	for _, tc := range testcases {
		actual := Start(tc.filepath, tc.class)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
