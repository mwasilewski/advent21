package day2

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

//marshallCourse reads a file and turns it into a golang object representing a course
func marshallCourse(filepath string) ([]mv, error) {
	var course []mv

	// open file
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open file: %w", err))
		return nil, err
	}
	defer fd.Close()

	// parse the file
	scanner := bufio.NewScanner(fd)
	for scanner.Scan() {
		move := mv{}
		fields := strings.Fields(scanner.Text())
		move.direction = fields[0]
		i, err := strconv.Atoi(fields[1])
		if err != nil {
			log.Println(fmt.Errorf("failed to convert string to int: %w", err))
			return nil, err
		}
		move.distance = i
		course = append(course, move)

	}
	if err := scanner.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan file: %w", err))
		return nil, err
	}

	// return course
	return course, nil
}
