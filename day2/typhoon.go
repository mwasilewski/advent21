package day2

import "log"

type subTyphoon struct {
	horiz int
	depth int
	aim   int
}

func (s *subTyphoon) Horiz() int {
	return s.horiz
}

func (s *subTyphoon) Depth() int {
	return s.depth
}

func (s *subTyphoon) Dive(course []mv) error {
	for _, v := range course {
		switch v.direction {
		case forwString:
			s.horiz += v.distance
			s.depth += s.aim * v.distance
		case downString:
			s.aim += v.distance
		case upString:
			s.aim -= v.distance
		default:
			log.Println("Should never be reached")
			panic("should never be reached")
		}
	}
	return nil
}
