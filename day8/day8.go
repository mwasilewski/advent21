package day8

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
)

type encoding struct {
	s map[string]int
	i map[int]string
}

type line struct {
	signals []string
	digits  []string
}

type lines []line

func loadData(filepath string) lines {
	var ls lines
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("Failed to open file: %w", err))
	}
	defer fd.Close()

	scannerLines := bufio.NewScanner(fd)
	for scannerLines.Scan() {
		l := line{}
		reachedDigits := false
		fields := strings.Fields(scannerLines.Text())
		for _, field := range fields {
			// sort chars in the field
			rs := []rune(field)
			sort.Slice(rs, func(i, j int) bool { // bubble sort using stdlib
				return rs[i] < rs[j]
			})
			field = string(rs)

			if field == "|" {
				reachedDigits = true
				continue
			}
			if reachedDigits {
				l.digits = append(l.digits, field)
				continue
			}
			l.signals = append(l.signals, field)
		}
		ls = append(ls, l)
	}
	if err := scannerLines.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan lines: %w", err))
	}

	return ls
}

func containsChars(s string, chars string) bool {
	contains := true
	for _, r := range chars {
		if !strings.Contains(s, string(r)) {
			contains = false
			break
		}
	}
	return contains
}

func removeChars(input string, characters string) string {
	filter := func(r rune) rune {
		if strings.IndexRune(characters, r) < 0 {
			return r
		}
		return -1
	}

	return strings.Map(filter, input)

}

func buildEncoding(signals []string) encoding {
	var localEncoding = encoding{
		s: make(map[string]int),
		i: make(map[int]string),
	}

	for _, signal := range signals {
		runes := []rune(signal)
		switch len(runes) {
		case 2:
			// 1
			localEncoding.s[signal] = 1
			localEncoding.i[1] = signal
		case 3:
			// 7
			localEncoding.s[signal] = 7
			localEncoding.i[7] = signal
		case 4:
			// 4
			localEncoding.s[signal] = 4
			localEncoding.i[4] = signal
		case 7:
			// 8
			localEncoding.s[signal] = 8
			localEncoding.i[8] = signal
		}
	}
	fourSubstring := removeChars(localEncoding.i[4], localEncoding.i[1])
	for _, signal := range signals {
		runes := []rune(signal)
		switch len(runes) {
		case 5:
			// 3
			if containsChars(signal, localEncoding.i[1]) {
				localEncoding.s[signal] = 3
				localEncoding.i[3] = signal
				continue
			}
			// 5
			if containsChars(signal, fourSubstring) {
				localEncoding.s[signal] = 5
				localEncoding.i[5] = signal
				continue
			}
			// 2
			localEncoding.s[signal] = 2
			localEncoding.i[2] = signal
		case 6:
			if containsChars(signal, localEncoding.i[1]) {
				if containsChars(signal, fourSubstring) {
					// 9
					localEncoding.s[signal] = 9
					localEncoding.i[9] = signal
					continue
				}
				localEncoding.s[signal] = 0
				localEncoding.i[0] = signal
				continue
			}
			// 6
			localEncoding.s[signal] = 6
			localEncoding.i[6] = signal
		}
	}

	return localEncoding
}

func decodeLine(l line) []int {
	var digitsInt []int
	e := buildEncoding(l.signals)
	for _, digit := range l.digits {
		digitsInt = append(digitsInt, e.s[digit])
	}
	return digitsInt
}

func countDigits(filepath string, targets []int) int {
	var cnt int
	ls := loadData(filepath)
	for _, l := range ls {
		digitsInt := decodeLine(l)
		for _, target := range targets {
			for _, digitInt := range digitsInt {
				if digitInt == target {
					cnt += 1
				}
			}
		}
	}
	return cnt
}

func sumNumbers(filepath string) int {
	var sum int
	ls := loadData(filepath)
	for _, l := range ls {
		var number int
		digitsInt := decodeLine(l)
		for _, v := range digitsInt {
			number = number*10 + v
		}
		sum += number
	}
	return sum
}
