package day8

import "testing"

type testCase struct {
	filepath    string
	targets     []int
	expected    int
	description string
}

var countDigitsTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		targets:     []int{1, 4, 7, 8},
		expected:    26,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		targets:     []int{1, 4, 7, 8},
		expected:    365,
		description: "actual puzzle",
	},
}

func TestCountDigits(t *testing.T) {
	for _, tc := range countDigitsTestCases {
		actual := countDigits(tc.filepath, tc.targets)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

var sumNumbersTestCases = []testCase{
	{
		filepath:    "./test_data/input1",
		expected:    61229,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    975706,
		description: "actual puzzle",
	},
}

func TestSumNumbers(t *testing.T) {
	for _, tc := range sumNumbersTestCases {
		actual := sumNumbers(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
