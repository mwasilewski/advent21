package day12

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

const (
	edgeSeparator = "-"

	pathStart = "start"
	pathEnd   = "end"
)

func isUpperCase(s string) bool {
	for _, r := range s {
		if int(r) >= 97 && int(r) <= 122 {
			return false
		}
	}
	return true
}

type Vertex struct {
	Label string
	Edges map[string]*Vertex
}

func NewVertex(label string) *Vertex {
	return &Vertex{
		Label: label,
		Edges: map[string]*Vertex{},
	}
}

type Graph struct {
	Vertices map[string]*Vertex
}

func NewGraph() *Graph {
	return &Graph{
		Vertices: map[string]*Vertex{},
	}
}

func (g *Graph) AddVertex(label string) error {
	if g.VertexExists(label) {
		return fmt.Errorf("vertex already exists")
	}
	v := NewVertex(label)
	g.Vertices[label] = v
	return nil
}

func (g Graph) VertexExists(label string) bool {
	_, exists := g.Vertices[label]
	return exists
}

func (g *Graph) AddEdge(l1 string, l2 string) error {
	if l1 == l2 {
		return fmt.Errorf("cannot create an edge with the same src and dst")
	}
	if !g.VertexExists(l1) || !g.VertexExists(l2) {
		return fmt.Errorf("one of the nodes in the given edge doesn't exist")
	}

	v1 := g.Vertices[l1]
	v2 := g.Vertices[l2]

	// add connection to V1
	_, v1connection := v1.Edges[l2]
	if !v1connection {
		v1.Edges[l2] = v2
	}

	// add connection to V2
	_, v2connection := v2.Edges[l1]
	if !v2connection {
		v2.Edges[l1] = v2

	}

	return nil
}

func simpleCheck(visited map[string]int, labelToCheck string) bool {
	if _, exists := visited[labelToCheck]; exists {
		return true
	}
	return false
}

func twiceCheck(visited map[string]int, labelToCheck string) bool {
	if labelToCheck == pathStart {
		return true
	}
	for _, v := range visited {
		if v == 2 {
			if _, exists := visited[labelToCheck]; exists {
				return true
			}
			return false
		}
	}
	return false
}

func (g Graph) dfsRecursive(start string, end string, visited map[string]int, currentPath []string, checkFunc func(map[string]int, string) bool) [][]string {
	currentPath = append(currentPath, start)
	if start == end {
		return [][]string{currentPath}
	}
	if !isUpperCase(start) {
		visited[start] += 1
	}

	var paths [][]string
	edges := g.Vertices[start].Edges
	for k, _ := range edges {
		if checkFunc(visited, k) {
			continue
		}
		var visitedCopy = make(map[string]int)
		for key, value := range visited {
			visitedCopy[key] = value
		}
		var currentPathCopy []string
		for _, value := range currentPath {
			currentPathCopy = append(currentPathCopy, value)
		}
		res := g.dfsRecursive(k, end, visitedCopy, currentPathCopy, checkFunc)
		paths = append(paths, res...)
	}
	return paths
}

func (g Graph) pathsDFS(start string, end string, checkFunc func(map[string]int, string) bool) [][]string {
	visited := map[string]int{}
	currentPath := []string{}
	paths := g.dfsRecursive(start, end, visited, currentPath, checkFunc)
	return paths
}

func loadGraphFromFile(filepath string) *Graph {
	fd, err := os.Open(filepath)
	if err != nil {
		log.Println(fmt.Errorf("failed to open a file: %w", err))
	}
	defer fd.Close()

	g := NewGraph()
	scannerLines := bufio.NewScanner(fd)
	for scannerLines.Scan() {
		line := scannerLines.Text()
		vs := strings.Split(line, edgeSeparator)
		if len(vs) > 2 {
			panic(fmt.Errorf("too many vertices in the edge definition"))
		}
		g.AddVertex(vs[0])
		g.AddVertex(vs[1])
		g.AddEdge(vs[0], vs[1])
	}
	if err := scannerLines.Err(); err != nil {
		log.Println(fmt.Errorf("failed to scan lines: %w", err))
	}

	return g
}

func noPaths(filepath string) int {
	g := loadGraphFromFile(filepath)
	paths := g.pathsDFS(pathStart, pathEnd, simpleCheck)
	return len(paths)
}

func noPathsTwice(filepath string) int {
	g := loadGraphFromFile(filepath)
	paths := g.pathsDFS(pathStart, pathEnd, twiceCheck)
	return len(paths)
}
