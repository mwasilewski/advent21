package day12

import "testing"

type testCase struct {
	filepath    string
	expected    int
	description string
}

var TCsNoPaths = []testCase{
	{
		filepath:    "./test_data/input0",
		expected:    10,
		description: "example",
	},

	{
		filepath:    "./test_data/input1",
		expected:    19,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    226,
		description: "example",
	},

	{
		filepath:    "./test_data/input3",
		expected:    4754,
		description: "actual puzzle",
	},
}

func TestNoPaths(t *testing.T) {
	for _, tc := range TCsNoPaths {
		actual := noPaths(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}

var TCsNoPathsTwice = []testCase{
	{
		filepath:    "./test_data/input0",
		expected:    36,
		description: "example",
	},

	{
		filepath:    "./test_data/input1",
		expected:    103,
		description: "example",
	},

	{
		filepath:    "./test_data/input2",
		expected:    3509,
		description: "example",
	},

	{
		filepath:    "./test_data/input3",
		expected:    143562,
		description: "actual puzzle",
	},
}

func TestNoPathsTwice(t *testing.T) {
	for _, tc := range TCsNoPathsTwice {
		actual := noPathsTwice(tc.filepath)
		if actual != tc.expected {
			t.Errorf("Expected: %d, got: %d", tc.expected, actual)
		}
	}
}
