package day15

import (
	"fmt"
	"gitlab.com/mwasilewski/advent21/matrices"
	"math"
)

var directions = []func(matrix matrices.Matrix, p *matrices.Point) (*matrices.Point, error){
	matrices.Matrix.Up,
	matrices.Matrix.Right,
	matrices.Matrix.Down,
	matrices.Matrix.Left,
}

type Path struct {
	points []matrices.Point
	sum    int
}

func isVisited(currentPath *Path, nextPoint *matrices.Point) bool {
	for _, point := range currentPath.points {
		if point.Y == nextPoint.Y && point.X == nextPoint.X {
			return true
		}
	}
	return false
}

func findAllPathsDFSRecursive(m matrices.Matrix, currentPoint *matrices.Point, endPoint *matrices.Point, currentPath Path) []Path {
	var paths []Path
	currentPath.points = append(currentPath.points, *currentPoint)
	currentPath.sum += m[currentPoint.Y][currentPoint.X]
	if currentPoint.X == endPoint.X && currentPoint.Y == endPoint.Y {
		fmt.Printf("found %v", currentPath)
		paths = append(paths, currentPath)
		return paths
	}
	for _, direction := range directions {
		nextPoint, err := direction(m, currentPoint)
		if err != nil {
			//log.Println(err)
			continue
		}
		if isVisited(&currentPath, nextPoint) {
			continue
		}
		path := findAllPathsDFSRecursive(m, nextPoint, endPoint, currentPath)
		paths = append(paths, path...)
	}
	return paths
}

func findLowestRisk(paths []Path) int {
	var min int
	for _, path := range paths {
		if min == 0 {
			min = path.sum
		}
		if path.sum < min {
			min = path.sum
		}
	}
	return min
}

func lowestRiskDFS(filepath string) int {
	// load input file
	m := matrices.Matrix{}
	m.NewDigitsFromFile(filepath)

	//initial state for the recursive function
	startingPoint := matrices.Point{
		X: 0,
		Y: 0,
	}
	endPoint := matrices.Point{
		X: m.NoCols() - 1, // points are 0 indexed
		Y: m.NoRows() - 1,
	}
	currentPath := Path{}
	// generate paths
	paths := findAllPathsDFSRecursive(m, &startingPoint, &endPoint, currentPath)
	min := findLowestRisk(paths)

	return min
}

/*


Dijkstra below


*/

type Point struct {
	pointRisk int
	totalRisk int
}

type RiskMatrix [][]*Point

func NewRiskMatrix(rows, cols int) RiskMatrix {
	var result RiskMatrix
	for y := 0; y < rows; y++ {
		row := []*Point{}
		for x := 0; x < cols; x++ {
			row = append(row, nil) // zero value
		}
		result = append(result, row)
	}
	return result
}

type SLLNode struct {
	point *Point
	next  *SLLNode
}

func (s *SLLNode) Add(newPoint *Point) {
	if s.next == nil {
		// at the end of the list
		newNode := &SLLNode{
			point: newPoint,
		}
		s.next = newNode
		return
	}
	if s.next.point.totalRisk < newPoint.totalRisk {
		s.next.Add(newPoint)
		return
	}
	newNode := &SLLNode{
		point: newPoint,
		next:  s.next,
	}
	s.next = newNode
}

//func lowestUnvisited(costs matrices.Matrix, visited [][]bool) *matrices.Point {
//	return nil
//}
//
//func findShortestPathDijkstra(m matrices.Matrix, endPoint *matrices.Point, costs matrices.Matrix, visited [][]bool) int {
//	for !visited[endPoint.Y][endPoint.X] {
//		currentPoint := lowestUnvisited(costs, visited)
//		neighbors := m.GetNeighbors(currentPoint, directions)
//		for _, neighbor := range neighbors {
//			if visited[neighbor.Y][neighbor.X] {
//				continue
//			}
//			// process neighbor
//		}
//
//	}
//
//	var paths []Path
//	currentPath.points = append(currentPath.points, *currentPoint)
//	currentPath.sum += m[currentPoint.Y][currentPoint.X]
//	if currentPoint.X == endPoint.X && currentPoint.Y == endPoint.Y {
//		fmt.Printf("found %v", currentPath)
//		paths = append(paths, currentPath)
//		return paths
//	}
//	for _, direction := range directions {
//		nextPoint, err := direction(m, currentPoint)
//		if err != nil {
//			//log.Println(err)
//			continue
//		}
//		if isVisited(&currentPath, nextPoint) {
//			continue
//		}
//		path := findAllPathsDFSRecursive(m, nextPoint, endPoint, currentPath)
//		paths = append(paths, path...)
//	}
//	return paths
//}

func lowestRiskDijkstra(riskPoints matrices.Matrix) int {
	riskTotal := matrices.NewMatrix(len(riskPoints[0]), len(riskPoints))
	riskTotal.AddToAll(math.MaxInt)

	visited := matrices.NewMatrixBool(len(riskPoints[0]), len(riskPoints))
	var unvisited []*matrices.Point
	for y := 0; y < len(riskPoints); y++ {
		for x := 0; x < len(riskPoints[0]); x++ {
			newPoint := &matrices.Point{
				X: x,
				Y: y,
			}
			unvisited = append(unvisited, newPoint)
		}
	}

	startingPoint := matrices.Point{
		X: 0,
		Y: 0,
	}
	endPoint := matrices.Point{
		X: riskPoints.NoCols() - 1, // points are 0 indexed
		Y: riskPoints.NoRows() - 1,
	}

	currentPoint := &matrices.Point{
		X: startingPoint.X,
		Y: startingPoint.Y,
	}
	riskTotal[currentPoint.Y][currentPoint.X] = 0 // starting point risk is not counted in
	for {
		neighbors := riskPoints.GetNeighbors(currentPoint, directions)
		for _, neighbor := range neighbors {
			if visited[neighbor.Y][neighbor.X] {
				continue
			}
			if riskTotal[neighbor.Y][neighbor.X] < riskTotal[currentPoint.Y][currentPoint.X]+riskPoints[neighbor.Y][neighbor.X] {
				continue
			}
			riskTotal[neighbor.Y][neighbor.X] = riskTotal[currentPoint.Y][currentPoint.X] + riskPoints[neighbor.Y][neighbor.X]

		}
		visited[currentPoint.Y][currentPoint.X] = true
		for i, point := range unvisited {
			if point.Y == currentPoint.Y && point.X == currentPoint.X {
				unvisited = append(unvisited[:i], unvisited[i+1:]...)
			}
		}
		if currentPoint.X == endPoint.X && currentPoint.Y == endPoint.Y {
			break
		}

		// find lowest unvisited and set it as the next current node
		var min *matrices.Point
		for _, point := range unvisited {
			if min == nil {
				min = point
				continue
			}
			if riskTotal[point.Y][point.X] < riskTotal[min.Y][min.X] {
				min = point
				continue
			}
		}
		currentPoint = min
	}
	lowestRisk := riskTotal[endPoint.Y][endPoint.X]
	return lowestRisk
}

func lowestRiskDijkstraSmall(filepath string) int {
	riskPoints := matrices.Matrix{}
	riskPoints.NewDigitsFromFile(filepath)
	return lowestRiskDijkstra(riskPoints)
}

func lowestRiskDijkstraBig(filepath string) int {
	multiplier := 5
	riskPointsSmall := matrices.Matrix{}
	riskPointsSmall.NewDigitsFromFile(filepath)
	riskPointsBig := matrices.NewMatrix(5*len(riskPointsSmall[0]), 5*len(riskPointsSmall))
	for y := 0; y < multiplier; y++ {
		for x := 0; x < multiplier; x++ {
			riskPointsStep := matrices.AddMatrices(
				riskPointsSmall,
				matrices.NewMatrix(1, 1),
				&matrices.Point{
					X: 0,
					Y: 0,
				},
			)
			riskPointsStep.AddToAll(x + y)
			riskPointsStep.TrimSubtract(9, 9)
			riskPointsBig = matrices.AddMatrices(riskPointsBig, riskPointsStep, &matrices.Point{
				X: x * riskPointsSmall.NoCols(),
				Y: y * riskPointsSmall.NoRows(),
			})
		}
	}
	return lowestRiskDijkstra(riskPointsBig)
}
