package day15

import (
	"testing"
)

func TestLowestRiskDFS(t *testing.T) {
	tcs := []struct {
		filepath    string
		expected    int
		description string
	}{
		{filepath: "./test_data/input0", expected: 8, description: "myexample"},
		{filepath: "./test_data/input1", expected: 40, description: "example"},
		{filepath: "./test_data/input2", expected: 1, description: "puzzle"},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			actual := lowestRiskDFS(tc.filepath)
			if actual != tc.expected {
				t.Errorf("expected: %d, got : %d", tc.expected, actual)
			}
		})
	}
}

func TestLowestRiskDijkstraSmall(t *testing.T) {
	tcs := []struct {
		filepath    string
		expected    int
		description string
	}{
		{filepath: "./test_data/input0", expected: 7, description: "myexample"},
		{filepath: "./test_data/input1", expected: 40, description: "example"},
		{filepath: "./test_data/input2", expected: 373, description: "puzzle"},
		{filepath: "./test_data/input3", expected: 315, description: "bigexample"},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			actual := lowestRiskDijkstraSmall(tc.filepath)
			if actual != tc.expected {
				t.Errorf("expected: %d, got : %d", tc.expected, actual)
			}
		})
	}
}

func TestLowestRiskDijkstraBig(t *testing.T) {
	tcs := []struct {
		filepath    string
		expected    int
		description string
	}{
		{filepath: "./test_data/input0", expected: 7, description: "myexample"},
		{filepath: "./test_data/input1", expected: 315, description: "example"},
		{filepath: "./test_data/input2", expected: 2868, description: "puzzle"},
	}

	for _, tc := range tcs {
		t.Run(tc.description, func(t *testing.T) {
			actual := lowestRiskDijkstraBig(tc.filepath)
			if actual != tc.expected {
				t.Errorf("expected: %d, got : %d", tc.expected, actual)
			}
		})
	}
}
